package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 */
public class Inventory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5291080253775847063L;
	private List<Item> inventory;

	public Inventory() {
		inventory = new ArrayList<Item>();
	}

	/**
	 * adds an item to the inventory
	 * 
	 * @param newItem
	 *            the item to add
	 */
	public void add(Item newItem) {
		inventory.add(newItem);
	}

	/**
	 * gets an item at the specified index
	 * 
	 * @param index
	 *            the index of the item to retrieve
	 */
	public Item get(int index) {
		return inventory.get(index);
	}

	/**
	 * removes an item from the inventory and returns that item
	 * 
	 * @param item
	 *            the item to remove and return at the end
	 */

	public Item remove(Item item) {
		Item removedItem = null;

		if (inventory.contains(item)) {
			int index = inventory.indexOf(item);
			removedItem = inventory.get(index);
			inventory.remove(index);
		}

		return removedItem;

	}

	/**
	 * returns if the inventory is empty
	 */
	public boolean isEmpty() {
		return inventory.isEmpty();
	}

	/**
	 * checks to see if the item exists in the inventory
	 * 
	 * @param item
	 *            the item to remove and return at the end
	 */

	public boolean contains(Item item) {
		return inventory.contains(item);
	}

	public List<Item> getAllItems() {
		return inventory;
	}

}