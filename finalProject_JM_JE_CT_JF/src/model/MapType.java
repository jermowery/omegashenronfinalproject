/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */
public enum MapType implements Serializable {
	EASY, MEDIUM, HARD, RANDOM, TEST;
}
