package model;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: The abstract class Unit that actual units inherit from
 * @Authors: Cindy Trieu, Jeremy Mowery, Jamel El-Merhaby, Jake Fritts
 */
@SuppressWarnings("serial")
public abstract class Unit implements Serializable {

	private List<StatNode> statList;
	private List<ActionNode> actions;
	private UnitType type;
	private Inventory inventory;
	private Team team;

	public Unit() {

		// Gets the defualt actions
		actions = UnitStrategy.getUnitActions(null);
		statList = UnitStrategy.getAllUnitStats();
		inventory = new Inventory();
		team = null;
	}

	public void useItem(Item item) {
		for (StatNode stat : statList) {
			for (StatNode itemSt : item.getStats()) {
				if (stat.getStatType() == itemSt.getStatType()) {
					setStat(stat.getStatType(), stat.getStatModification()
							+ itemSt.getStatModification());
				}
			}
		}
		inventory.remove(item);
		System.out.println("I am removing an item");
	}

	public List<ActionNode> getActions() {
		return actions;
	}

	public List<StatNode> getStatList() {
		return statList;
	}

	public void setStatList(List<StatNode> statList) {
		this.statList = statList;
	}

	public void setActions(List<ActionNode> actions) {
		this.actions = actions;
	}

	public void setType(UnitType type) {
		this.type = type;
	}

	public UnitType getType() {
		return type;
	}

	public Inventory getInventory() {
		return inventory;
	}

	/**
	 * Add an item to the Inventory
	 * 
	 * @param item
	 *            the item to add
	 */
	public void addToInventory(Item item) {
		inventory.add(item);
	}

	/**
	 * Remove an item from this unit's inventory
	 * 
	 * @param item
	 *            the item to remove
	 * @return the item removed
	 */
	public Item remove(Item item) {
		return inventory.remove(item);
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * Gets the stat modification for the type passed in.
	 * 
	 * @param type
	 *            the stat type to get the mofication for
	 * @return the stat modification
	 */
	public int getStat(StatType type) {
		int result = -1;
		for (int index = 0; index < statList.size(); index++) {
			if (statList.get(index).getStatType() == type) {
				result = statList.get(index).getStatModification();
				break;
			}
		}
		return result;
	}

	/**
	 * Sets the type passed in to the new value
	 * 
	 * @param type
	 *            the type to change
	 * @param newValue
	 *            the value to change to
	 */
	public void setStat(StatType type, int newValue) {
		for (StatNode s : statList) {
			if (s.getStatType() == type) {
				s.setStatModification(newValue);
				break;
			}
		}
	}

	public boolean isEnemy(Team team) {
		return this.team != team;
	}

	public String getName() {
		return UnitStrategy.getName(type);
	}

	public String getSpecial() {
		return ActionStrategy.getSpecialAsString(type);
	}
}
