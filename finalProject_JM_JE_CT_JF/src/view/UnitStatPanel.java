/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.GridLayout;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class UnitStatPanel extends AbstractStatPanel {

	/**
	 * 
	 */
	public UnitStatPanel() {
		setPreferredSize(new Dimension(305, 125));
		setLayout(new GridLayout(ViewStrategy.UNIT_NUM_ROWS,
				ViewStrategy.UNIT_NUM_COLS));
	}

}
