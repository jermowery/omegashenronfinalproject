package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class DrMercer extends Unit implements Serializable {
	public DrMercer() {
		super();
		setType(UnitType.DRMERCER);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.DRMERCER));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.DRMERCER));
		super.setStatList(stats);
	}
}
