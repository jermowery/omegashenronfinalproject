/**
 * 
 */
package view;

import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import model.Inventory;
import model.Item;
import model.StatNode;
import model.StatType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class InventoryTableModel implements TableModel {

	Inventory inventory;
	List<Item> itemList;

	/**
	 * @param inventory
	 *            TODO
	 * 
	 */
	public InventoryTableModel(Inventory inventory) {
		super();
		this.inventory = inventory;
		itemList = inventory.getAllItems();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return itemList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return 8;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return "Name";
		case 1:
			return "Attack";
		case 2:
			return "HP";
		case 3:
			return "Movement";
		case 4:
			return "Defense";
		case 5:
			return "Attack Range";
		case 6:
			return "Action Points";
		case 7:
			return "Special Action Points Usage";
		default:
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return String.class;
		default:
			return int.class;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Item currentItem = itemList.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return currentItem.getName();
		case 1:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.ATTACK) {
					return s.getStatModification();
				}
			}
			return 0;
		case 2:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.HP) {
					return s.getStatModification();
				}
			}
			return 0;
		case 3:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.MOVEMENT) {
					return s.getStatModification();
				}
			}
			return 0;
		case 4:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.DEFENSE) {
					return s.getStatModification();
				}
			}
			return 0;
		case 5:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.ATTACK_RANGE) {
					return s.getStatModification();
				}
			}
			return 0;
		case 6:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.ACTION_POINTS) {
					return s.getStatModification();
				}
			}
			return 0;
		default:
			for (StatNode s : currentItem.getStats()) {
				if (s.getStatType() == StatType.SPECIAL_ACTION_USAGE) {
					return s.getStatModification();
				}
			}
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableModel#addTableModelListener(javax.swing.event.
	 * TableModelListener)
	 */
	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.TableModel#removeTableModelListener(javax.swing.event
	 * .TableModelListener)
	 */
	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

}
