/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This class must be a singleton
 */
@SuppressWarnings("serial")
public class Commander extends Unit implements Serializable {

	private static Commander instance = null;

	public static Commander getInstance() {
		if (instance == null) {
			instance = new Commander();
			return instance;
		} else {
			return instance;
		}
	}

	private Commander() {
		super();
		setType(UnitType.COMMANDER);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.COMMANDER));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.COMMANDER));
		super.setStatList(stats);
	}
}
