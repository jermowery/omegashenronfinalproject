package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Weaver extends Unit implements Serializable {

	/**
	 * Creates a new Weaver
	 */
	public Weaver() {
		super();
		setType(UnitType.WEAVER);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.WEAVER));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.WEAVER));
		super.setStatList(stats);
	}

}
