/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 */
public enum TileType implements Serializable {
	RUBBLE, WALL, DESERT, WATER, ITEM, TESLA_COIL;

}
