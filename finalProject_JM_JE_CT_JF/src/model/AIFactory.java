/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class AIFactory implements Serializable {
	public AIFactory(AIType type) {
		generate(type);
	}

	public List<Unit> generate(AIType type) {
		List<Unit> temp = new ArrayList<Unit>();

		switch (type) {
		case EASY:
			AnnoyingBrat brat1 = new AnnoyingBrat();
			temp.add(brat1);
			AnnoyingBrat brat2 = new AnnoyingBrat();
			temp.add(brat2);
			RoidRageDiver diver1 = new RoidRageDiver();
			temp.add(diver1);
			RoidRageDiver diver2 = new RoidRageDiver();
			temp.add(diver2);
			Tesla tesla1 = new Tesla();
			temp.add(tesla1);
			break;
		case MEDIUM:
			// TODO: make enemies
			break;
		case HARD:
			// TODO: make enemies
			break;
		default:
			break;
		}

		return temp;
	}
}
