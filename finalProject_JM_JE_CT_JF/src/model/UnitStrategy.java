/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class UnitStrategy implements Serializable {

	public static int MAX_NUMBER_OF_UNITS = 5;

	/**
	 * When implemented this stragegy will get the unit actions for any unit
	 * passed in
	 * 
	 * @param unit
	 *            the unit to get actions for
	 * @return a list of actions that the unit can perform
	 */
	public static List<ActionNode> getUnitActions(UnitType unit) {

		if (unit == UnitType.ANNOYINGBRAT) {
			return getAnnoyingBratActions();
		} else if (unit == UnitType.DRMERCER) {
			return getDrMercerActions();
		} else if (unit == UnitType.DYLAN) {
			return getDylanActions();
		} else if (unit == UnitType.GEARHEAD) {
			return getGearheadActions();
		} else if (unit == UnitType.ROIDRAGEDIVER) {
			return getRoidRageDiverActions();
		} else if (unit == UnitType.TESLA) {
			return getTeslaActions();
		} else if (unit == UnitType.TROOPER) {
			return getTrooperActions();
		} else if (unit == UnitType.WEAVER) {
			return getWeaverActions();
		} else if (unit == UnitType.ERIC) {
			return getEricActions();
		} else if (unit == UnitType.COMMANDER) {
			return getCommanderActions();
		} else {
			return getAllUnitActions();
		}

	}

	private static List<ActionNode> getCommanderActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.COMMANDER_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.COMMANDER_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getEricActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.ERIC_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.ERIC_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getWeaverActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.WEAVER_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.WEAVER_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getTrooperActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.TROOPER_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.TROOPER_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getTeslaActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.TESLA_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.TESLA_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getRoidRageDiverActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.ROID_RAGE_DIVER_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.ROID_RAGE_DIVER_POINTS));
		return actions;
	}

	private static List<ActionNode> getGearheadActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.GEARDHEAD_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.GEARHEARD_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getDylanActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.DYLAN_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.DYLAN_ACTION_POINTS));
		return actions;
	}

	private static List<ActionNode> getDrMercerActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.DRMERCER_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.DRMERCER_POINTS));
		return actions;
	}

	private static List<ActionNode> getAnnoyingBratActions() {
		List<ActionNode> actions = new LinkedList<>();
		actions.add(new ActionNode(ActionStrategy.ANNOYING_BRAT_SPECIAL,
				ActionType.SPECIAL, ActionStrategy.ANNOYING_BRAT_ACTION_POINTS));
		return actions;
	}

	/**
	 * Gets the actions that all units have
	 * 
	 * @return a list of all the actions that every unit can perform
	 */
	private static List<ActionNode> getAllUnitActions() {
		List<ActionNode> result = new LinkedList<>();
		result.add(new ActionNode("Move", ActionType.MOVE,
				ActionStrategy.MOVEMENT_POINTS));
		result.add(new ActionNode("Attack", ActionType.ATTACK,
				ActionStrategy.ATTACK_POINTS));
		result.add(new ActionNode("Do Nothing", ActionType.NOTHING,
				ActionStrategy.NOTHING_POINTS));
		result.add(new ActionNode("Shop", ActionType.SHOP,
				ActionStrategy.SHOP_POINTS));
		result.add(new ActionNode("Inventory", ActionType.INVENTORY,
				ActionStrategy.INVENTORY_POINTS));
		result.add(new ActionNode("Trade", ActionType.TRADE,
				ActionStrategy.TRADE_POINTS));
		// result.add(new ActionNode("Trade", ActionType.TRADE,
		// ActionStrategy.TRADE_POINTS));

		return result;
	}

	/**
	 * Gets the default stats for the unit passed in
	 * 
	 * @param unit
	 *            the unit type to get the stats for
	 * @return a list of StatNode representing each of the stats
	 */
	public static List<StatNode> getUnitDefaultStats(UnitType unit) {
		if (unit == UnitType.ANNOYINGBRAT) {
			return getAnnoyingBratDefaultStats();
		} else if (unit == UnitType.DRMERCER) {
			return getDrMercerDefaultStats();
		} else if (unit == UnitType.DYLAN) {
			return getDylanDefaultStats();
		} else if (unit == UnitType.GEARHEAD) {
			return getGearheadDefaultStats();
		} else if (unit == UnitType.ROIDRAGEDIVER) {
			return getRoidRageDiverDefaultStats();
		} else if (unit == UnitType.TESLA) {
			return getTeslaDefaultStats();
		} else if (unit == UnitType.TROOPER) {
			return getTrooperDefaultStats();
		} else if (unit == UnitType.WEAVER) {
			return getWeaverDefaultStats();
		} else if (unit == UnitType.ERIC) {
			return getEricDefaultStats();
		} else if (unit == UnitType.COMMANDER) {
			return getCommanderDefaultStats();
		} else {
			return null;
		}
	}

	private static List<StatNode> getCommanderDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.COMMANDER_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.COMMANDER_HP));
		result.add(new StatNode(StatType.DEFENSE,
				StatStrategy.COMMANDER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.COMMANDER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.COMMANDER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.COMMANDER_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.COMMANDER_SPECIAL_ACTION_USAGE));
		return result;
	}

	private static List<StatNode> getEricDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.ERIC_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.ERIC_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.ERIC_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, StatStrategy.ERIC_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.ERIC_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.ERIC_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.ERIC_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the weaver
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getWeaverDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.WEAVER_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.WEAVER_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.WEAVER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, StatStrategy.WEAVER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.WEAVER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.WEAVER_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.WEAVER_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the trooper
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getTrooperDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.TROOPER_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.TROOPER_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.TROOPER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.TROOPER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.TROOPER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.TROOPER_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.TROOPER_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the tesla
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getTeslaDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.TESLA_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.TESLA_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.TESLA_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, StatStrategy.TESLA_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.TESLA_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.TESLA_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.TESLA_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the RoidRageDiver
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getRoidRageDiverDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK,
				StatStrategy.ROID_RAGE_DIVER_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.ROID_RAGE_DIVER_HP));
		result.add(new StatNode(StatType.DEFENSE,
				StatStrategy.ROID_RAGE_DIVER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.ROID_RAGE_DIVER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.ROID_RAGE_DIVER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.ROID_RAGE_DIVER_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.ROID_RAGE_DIVER_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the Gearhead
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getGearheadDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.GEARHEAD_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.GEARHEAD_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.GEARHEAD_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.GEARHEAD_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.GEARHEAD_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.GEARHEAD_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.GEARHEAD_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the Dylan
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getDylanDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.DYLAN_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.DYLAN_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.DYLAN_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, StatStrategy.DYLAN_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.DYLAN_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.DYLAN_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.DYLAN_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the Dr Mercer
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getDrMercerDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, StatStrategy.DRMERCER_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.DRMERCER_HP));
		result.add(new StatNode(StatType.DEFENSE, StatStrategy.DRMERCER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.DRMERCER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.DRMERCER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.DRMERCER_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.DRMERCER_SPECIAL_ACTION_USAGE));
		return result;
	}

	/**
	 * Gets the default stats for the annoying brat
	 * 
	 * @return a list of default stats
	 */
	private static List<StatNode> getAnnoyingBratDefaultStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK,
				StatStrategy.ANNOYING_BRAT_ATTACK));
		result.add(new StatNode(StatType.HP, StatStrategy.ANNOYING_BRAT_HP));
		result.add(new StatNode(StatType.DEFENSE,
				StatStrategy.ANNOYING_BRAT_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				StatStrategy.ANNOYING_BRAT_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				StatStrategy.ANNOYING_BRAT_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				StatStrategy.ANNOYING_BRAT_ACTION_POINTS));
		result.add(new StatNode(StatType.SPECIAL_ACTION_USAGE,
				StatStrategy.ANNOYING_BRAT_SPECIAL_ACTION_USAGE));
		return result;
	}

	public static List<StatNode> getAllUnitStats() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.EXPERIENCE,
				StatStrategy.DEFAULT_EXPERIENCE));
		return result;
	}

	public static String getName(UnitType type) {
		switch (type) {
		case ANNOYINGBRAT:
			return "Annoying Brat";
		case COMMANDER:
			return "Commander";
		case DRMERCER:
			return "Dr. Mercer";
		case DYLAN:
			return "Dylan";
		case ERIC:
			return "Eric";
		case GEARHEAD:
			return "Gearhead";
		case ROIDRAGEDIVER:
			return "Roid Rage Diver";
		case TESLA:
			return "Tesla";
		case TROOPER:
			return "Trooper";
		case WEAVER:
			return "Weaver";
		default:
			return "Impossible";

		}

	}
}
