/**
 * 
 */
package view;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

import model.ActionType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         A button that contains an action
 */
@SuppressWarnings("serial")
public class ActionButton extends JButton {

	ActionType actionType;

	/**
	 * 
	 */
	public ActionButton(String name, ActionType actionType) {
		super();
		this.setToolTipText(name);
		setActionType(actionType);
	}

	/**
	 * @param icon
	 */
	public ActionButton(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param name
	 */
	public ActionButton(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param a
	 */
	public ActionButton(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param text
	 * @param icon
	 */
	public ActionButton(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public ActionType getActionType() {
		return actionType;
	}

}
