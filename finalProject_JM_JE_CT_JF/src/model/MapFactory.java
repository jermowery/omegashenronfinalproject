package model;

import java.io.Serializable;
import java.util.Random;

import tiles.Desert;
import tiles.Rubble;
import tiles.Wall;
import tiles.Water;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 * @description: class using the factory implementation to generate new maps
 */
@SuppressWarnings("serial")
public class MapFactory implements Serializable {
	static Tile grid[][];

	/**
	 * generating a base desert map, and each static method will change the
	 * tiles based on a certain map design
	 * 
	 */

	private static void generateBaseMap() {
		Random rand = new Random();

		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[0].length; col++) {
				int generateValue = rand.nextInt(4);
				switch (generateValue) {

				case 0:
				case 1:
					grid[row][col] = new Desert(TileType.DESERT);
					break;
				case 2:
					grid[row][col] = new Rubble(TileType.RUBBLE);
					break;
				case 3:
					grid[row][col] = new Water(TileType.WATER);
					break;
				default:
					System.out.println("ERROR! UKNOWN VALUE IN MAP GENERATION");
				}
			}
		}

	}

	public static Tile[][] getTestMap() {
		grid = new Tile[MapStrategy.MEDIUM_MAP_SIZE][MapStrategy.MEDIUM_MAP_SIZE];
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[0].length; col++) {

				grid[row][col] = new Desert(TileType.DESERT);

			}
		}

		return grid;

	}

	/**
	 * generating an easy map based on a design
	 * 
	 */
	public static Tile[][] getEasyMap() {
		grid = new Tile[MapStrategy.EASY_MAP_SIZE][MapStrategy.EASY_MAP_SIZE];

		generateBaseMap();

		grid[10][9] = new Wall(TileType.WALL);
		grid[11][10] = new Wall(TileType.WALL);
		grid[13][9] = new Wall(TileType.WALL);
		grid[12][9] = new Wall(TileType.WALL);
		grid[9][4] = new Wall(TileType.WALL);
		grid[3][17] = new Wall(TileType.WALL);

		return grid;
	}

	/**
	 * generating a medium map based on a design
	 * 
	 */
	public static Tile[][] getMediumMap() {
		grid = new Tile[MapStrategy.MEDIUM_MAP_SIZE][MapStrategy.MEDIUM_MAP_SIZE];
		generateBaseMap();

		grid[15][18] = new Wall(TileType.WALL);
		grid[13][19] = new Wall(TileType.WALL);
		grid[14][13] = new Wall(TileType.WALL);
		grid[14][20] = new Wall(TileType.WALL);
		grid[12][10] = new Wall(TileType.WALL);
		grid[16][13] = new Wall(TileType.WALL);
		grid[18][9] = new Wall(TileType.WALL);
		grid[9][11] = new Wall(TileType.WALL);

		return grid;

	}

	/**
	 * generating a hard map based on a design
	 * 
	 */
	public static Tile[][] getHardMap() {
		grid = new Tile[MapStrategy.HARD_MAP_SIZE][MapStrategy.HARD_MAP_SIZE];
		generateBaseMap();

		/**
		 * hard coding a hard to traverse wall near the center of the map
		 * 
		 */
		grid[15][16] = new Wall(TileType.WALL);
		grid[16][15] = new Wall(TileType.WALL);
		grid[17][15] = new Wall(TileType.WALL);
		grid[18][15] = new Wall(TileType.WALL);
		grid[19][14] = new Wall(TileType.WALL);
		grid[20][23] = new Wall(TileType.WALL);
		grid[7][14] = new Wall(TileType.WALL);
		grid[12][16] = new Wall(TileType.WALL);
		grid[13][17] = new Wall(TileType.WALL);
		grid[14][13] = new Wall(TileType.WALL);
		grid[10][12] = new Wall(TileType.WALL);
		grid[4][13] = new Wall(TileType.WALL);
		grid[8][10] = new Wall(TileType.WALL);
		grid[5][17] = new Wall(TileType.WALL);
		grid[17][23] = new Wall(TileType.WALL);
		grid[9][7] = new Wall(TileType.WALL);

		return grid;
	}

	/**
	 * generating a completely random map, with a max wall count to avoid tons
	 * of walls.
	 */
	public static Tile[][] getRandomMap() {
		Tile grid[][] = new Tile[MapStrategy.RANDOM_MAP_SIZE][MapStrategy.RANDOM_MAP_SIZE];
		Random rand = new Random();
		int generateValue = rand.nextInt(5);
		int wallCount = 20;

		/**
		 * desert has more values to avoid tons of difficult tiles being
		 * generated
		 */

		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[0].length; col++) {

				switch (generateValue) {

				case 0:
				case 1:
					grid[row][col] = new Desert(TileType.DESERT);
					break;
				case 2:
					grid[row][col] = new Rubble(TileType.RUBBLE);
					break;
				case 3:
					grid[row][col] = new Water(TileType.WATER);
					break;
				case 4:
					if (wallCount != 0) {
						grid[row][col] = new Wall(TileType.WALL);
						wallCount--;
					} else
						grid[row][col] = new Rubble(TileType.RUBBLE);
					break;
				default:
					System.out
							.println("ERROR! UKNOWN VALUE IN RANDOM MAP GENERATION");
				}
			}

		}

		return grid;
	}
}
