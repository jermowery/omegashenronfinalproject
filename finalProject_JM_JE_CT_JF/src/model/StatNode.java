/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         Defines a stat node represent a type of a stat represented as a
 *         String and a number to modify
 */
@SuppressWarnings("serial")
public class StatNode implements Serializable {
	private StatType statType;
	private String statString;
	private int statModification;

	/**
	 * Constructs a new StatNode
	 * 
	 * @param attack
	 *            the type of the this stat
	 * @param statModification
	 *            the modification to apply to the stat
	 */
	public StatNode(StatType type, int statModification) {
		this.statType = type;
		this.statModification = statModification;
		this.statString = StatStrategy.getStatName(statType);
	}

	/**
	 * Gets the stat type
	 * 
	 * @return the statType
	 */
	public String getStatNameType() {
		return statString;
	}

	/**
	 * Gets the modifcation
	 * 
	 * @return the Modficication
	 */
	public int getStatModification() {
		return statModification;
	}

	public StatType getStatType() {
		return statType;
	}

	public void setStatModification(int statModification) {
		this.statModification = statModification;
	}
}
