package test;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */

import java.util.LinkedList;
import java.util.List;

import model.Game;
import model.MapType;
import model.Player;
import model.RoidRageDiver;
import model.ScenarioType;
import model.Team;
import model.Trooper;
import model.Unit;
import model.Weaver;

import org.junit.Test;

public class testGame {

	@Test
	public void testCreationAndInitialVariables() {

	}

	@Test
	public void testACustomMap() {
		Player player = new Player();
		List<Unit> playerUnits = new LinkedList<>();
		playerUnits.add(new Weaver());
		playerUnits.get(0).setTeam(Team.PLAYER);
		player.setUnits(playerUnits);
		Game game = Game.getInstance();
		game.setCurrentPlayer(player);
		game.setScenario(ScenarioType.KILL_COMMANDER);
		game.setMap(MapType.EASY);
		System.out.println(game.toString());

		for (int i = 0; i < 1000; i++) {
			game.resetGame();
			game = Game.getInstance();
			game.setCurrentPlayer(player);
			game.setScenario(ScenarioType.KILL_COMMANDER);
			game.setMap(MapType.EASY);
			while (!game.isGameOver()) {
				game.endTurn();
			}
			// System.out.println(game.toString());
			System.out.println(i);
		}

		playerUnits = new LinkedList<>();
		playerUnits.add(new Weaver());
		playerUnits.get(0).setTeam(Team.PLAYER);
		playerUnits.add(new Trooper());
		playerUnits.get(1).setTeam(Team.PLAYER);
		playerUnits.add(new RoidRageDiver());
		playerUnits.get(2).setTeam(Team.PLAYER);
		player.setUnits(playerUnits);

		for (int i = 0; i < 1000; i++) {
			game.resetGame();
			game = Game.getInstance();
			game.setCurrentPlayer(player);
			game.setScenario(ScenarioType.KILL_COMMANDER);
			game.setMap(MapType.EASY);
			while (!game.isGameOver()) {
				game.endTurn();
			}
			// System.out.println(game.toString());
			System.out.println(i);
		}
	}

}
