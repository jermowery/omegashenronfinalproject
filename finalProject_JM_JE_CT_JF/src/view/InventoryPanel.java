/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Game;
import model.GameState;
import model.Inventory;
import model.Item;
import model.Unit;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This is the panel shown for a unit's inventory
 */
@SuppressWarnings("serial")
public class InventoryPanel extends JPanel {

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class InventoryTableListener implements ListSelectionListener {

		private ItemImagePanel imagePanel;

		public InventoryTableListener(ItemImagePanel imagePanel) {
			this.imagePanel = imagePanel;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * javax.swing.event.ListSelectionListener#valueChanged(javax.swing.
		 * event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			select.setEnabled(true);
			int row = inventoryTable.getSelectedRow();
			Item selectedItem = inventory.get(row);
			imagePanel.setImage(ImageFactory.getInstance().getItemImage(
					selectedItem.getType()));
			imagePanel.repaint();
		}
	}

	public class SelectedButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int row = inventoryTable.getSelectedRow();
			Item selectedItem = inventory.get(row);
			inventory.remove(inventory.get(row));
			if (Game.getInstance().getState() == GameState.SELECTING_ITEMS) {
				Game.getInstance().trade(
						Game.getInstance().getSelectedTile().getCurrentUnit(),
						unitTradingTo, selectedItem);
			} else {
				Game.getInstance().useItem(selectedItem);
			}
			select.setEnabled(false);
		}
	}

	private Unit unitTradingTo;
	/**
	 * The table of all items in the players inventory
	 */
	private JTable inventoryTable;
	/**
	 * The select button that confirms the selection of an item
	 */
	private JButton select;

	private Inventory inventory;

	private ItemImagePanel imagePanel;

	/**
	 * Use this constructor to start the panel with elements
	 * 
	 * @param inventory
	 *            TODO
	 */
	public InventoryPanel(Inventory inventory) {
		this.inventory = inventory;
		initializeComponents();
		addComponentsToScreen();

	}

	/**
	 * Initializes instance variables to default values
	 */
	private void initializeComponents() {
		initializeTable();
		initializeButtons();
		initializePanels();
	}

	/**
	 * Adds components to the screen
	 */
	private void addComponentsToScreen() {
		this.setLayout(new BorderLayout());
		imagePanel.setPreferredSize(new Dimension(50, 200));
		this.add(imagePanel, BorderLayout.WEST);
		inventoryTable.setPreferredSize(new Dimension(700, 400));
		this.add(inventoryTable, BorderLayout.EAST);
		select.setPreferredSize(new Dimension(100, 50));
		this.add(select, BorderLayout.SOUTH);

	}

	/**
	 * Initializes all panels.
	 */
	private void initializePanels() {
		imagePanel = new ItemImagePanel();
	}

	/**
	 * 
	 */
	private void initializeTable() {
		inventoryTable = new JTable(new InventoryTableModel(inventory));
		inventoryTable.getSelectionModel().addListSelectionListener(
				new InventoryTableListener(imagePanel));
	}

	/**
	 * Initializes all tables
	 */
	private void initializeButtons() {
		select = new JButton("Select");
		select.setEnabled(false);
		attachActionListeners();
	}

	/**
	 * Unites components with the appropriate action listener
	 */
	private void attachActionListeners() {
		select.addActionListener(new SelectedButtonListener());
	}

	public void setInventory(Inventory inventory) {
		this.remove(inventoryTable);
		this.inventory = inventory;
		if (inventory.getAllItems().size() == 0) {
			select.setEnabled(false);
		}
		inventoryTable = new JTable(new InventoryTableModel(inventory));
		inventoryTable.getSelectionModel().addListSelectionListener(
				new InventoryTableListener(imagePanel));
		this.add(inventoryTable, 1);
		revalidate();
		repaint();
	}

	public void setUnitTradingTo(Unit unit) {
		this.unitTradingTo = unit;
	}
}
