/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Tile;
import model.TileStrategy;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This panel displays the stats
 */
@SuppressWarnings("serial")
public class StatsPanel extends JPanel {

	AbstractStatPanel terrainStatPanel;
	AbstractStatPanel unitStatPanel;

	/**
	 * Use this constructor to start the panel off with the elements
	 */
	public StatsPanel() {

		super();
		setLayout(new GridLayout(2, 1));
		unitStatPanel = new UnitStatPanel();
		terrainStatPanel = new TerrainStatPanel();

		this.setPreferredSize(new Dimension(305, 225));
		this.add(terrainStatPanel);
		this.add(unitStatPanel);

	}

	/**
	 * @param layout
	 */
	public StatsPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public StatsPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public StatsPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	public void updateStats(Tile tile) {
		if (tile == null) {
			terrainStatPanel.removeAll();
			unitStatPanel.removeAll();
			terrainStatPanel.repaint();
			unitStatPanel.repaint();
		} else {
			BufferedImage terrainImage = ImageFactory.getInstance()
					.getTerrainImage(tile.getTileType());
			terrainStatPanel.setSprite(terrainImage);
			terrainStatPanel.setNameLabel(new JLabel(TileStrategy.getName(tile
					.getTileType())));
			terrainStatPanel.updateStats(tile.getStatModifiers());
			if (tile.getCurrentUnit() != null) {
				BufferedImage unitImage = ImageFactory.getInstance()
						.getUnitDefaultImage(tile.getCurrentUnit().getType());
				unitStatPanel.setSprite(unitImage);
				unitStatPanel.setNameLabel(new JLabel(tile.getCurrentUnit()
						.getName()));
				unitStatPanel.updateStats(tile.getCurrentUnit().getStatList());
			} else {
				unitStatPanel.removeStats();
			}
		}
	}

}
