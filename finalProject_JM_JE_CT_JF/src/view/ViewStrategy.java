/**
 * 
 */
package view;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This strategy is for the constants we need in the GUI: sizes,
 *         locations, etc...
 */
public class ViewStrategy {
	/**
	 * Defines the height of each tile
	 */
	public final static int TILE_HEIGHT = 50;
	/**
	 * Defines the width of each tile
	 */
	public final static int TILE_WIDTH = 50;

	public static final int TERRAIN_NUM_COLS = 2;
	public static final int TERRAIN_NUM_ROWS = 7;
	public static final int UNIT_NUM_ROWS = 9;
	public static final int UNIT_NUM_COLS = 2;

}
