/**
 * 
 */
package view;

import java.awt.Dimension;

import javax.swing.JLabel;

import model.StatStrategy;
import model.StatType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class DoubleLabel {

	private JLabel statName;
	private JLabel statValue;

	/**
	 * 
	 */
	public DoubleLabel(StatType type, int statValue) {
		statName = new JLabel();
		statName.setText(StatStrategy.getStatName(type));
		this.statValue = new JLabel();
		this.statValue.setText(statValue + "");
		this.statName.setPreferredSize(new Dimension(50, 25));
		this.statValue.setPreferredSize(new Dimension(50, 25));
	}

	public JLabel getStatName() {
		return statName;
	}

	public JLabel getStatValue() {
		return statValue;
	}

}
