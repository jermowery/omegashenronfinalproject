/**
 * 
 */
package model;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class GameStrategy {
	
	/**
	 * First win condition
	 */
	public static final String PRIMARY_GAME_OBJECTIVE = "Kill enemy commander!";
	/**
	 * Second win condition
	 */
	public static final String SECONDARY_GAME_OBJECTIVE = "Kill all enemies!";
	/**
	 * Third win condition
	 */
	public static final String TERTIARY_GAME_OBJECTIVE = "Collect all seven Dragon Balls!";
}
