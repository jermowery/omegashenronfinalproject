/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author jamel
 * 
 */
@SuppressWarnings("serial")
public class PlayerStrategy implements Serializable {

	// This number should be changed but for now it is 0.
	public static final int STARTING_WEALTH = 20;
}
