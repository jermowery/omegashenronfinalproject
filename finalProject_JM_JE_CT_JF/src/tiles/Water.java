package tiles;

import java.io.Serializable;

import model.Tile;
import model.TileType;

@SuppressWarnings("serial")
public class Water extends Tile implements Serializable {

	public Water(TileType type) {
		super(type);

	}

}
