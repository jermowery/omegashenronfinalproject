/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 */
public enum ScenarioType implements Serializable {
	BUY_DRAGONBALLS, KILL_ALL_ENEMIES, KILL_COMMANDER, ESCAPE
}
