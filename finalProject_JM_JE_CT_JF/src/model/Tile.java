/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */
@SuppressWarnings("serial")
public abstract class Tile implements Serializable {

	/**
	 * The list of all statModifiers for a tile.
	 */
	private List<StatNode> statModifiers;

	private List<ActionNode> tileActions;
	/**
	 * the type of tile
	 */
	private TileType tileType;

	/**
	 * the currently selected unit.
	 */
	private Unit currentUnit;

	public Tile(TileType type) {
		statModifiers = new ArrayList<>();
		this.tileType = type;
		this.currentUnit = null;
		// Now the stat modifers are determined by the stategy, this makes it
		// much easier to change later, and with the inharitance I've set up it
		// makes it much easier to collect information
		statModifiers = TileStrategy.getStatModifications(type);
		tileActions = TileStrategy.getTileActions(type);
	}

	/**
	 * Gets the current unit on the tile
	 * 
	 * @return
	 */
	public Unit getCurrentUnit() {
		return currentUnit;
	}

	/**
	 * Gets the statModifers for this tile
	 * 
	 * @return the stat modifers
	 */
	public List<StatNode> getStatModifiers() {
		return statModifiers;
	}

	/**
	 * Sets the statModifiers for this tile
	 * 
	 * @param statModifiers
	 */

	public void setStatModifiers(List<StatNode> statModifiers) {
		this.statModifiers = statModifiers;
	}

	/**
	 * Gets the a List of ActionNode for the this tile
	 * 
	 * @return the tileActions
	 */
	public List<ActionNode> getTileActions() {

		if (currentUnit != null) {
			List<ActionNode> actions = new ArrayList<>();
			actions.addAll(currentUnit.getActions());
			return actions;
		} else {
			return tileActions;
		}
	}

	public TileType getTileType() {
		return tileType;
	}

	public void setCurrentUnit(Unit currentUnit) {
		this.currentUnit = currentUnit;
	}

	public String getName() {
		return TileStrategy.getName(tileType);
	}

}
