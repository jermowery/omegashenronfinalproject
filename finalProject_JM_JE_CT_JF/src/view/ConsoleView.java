package view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import javax.swing.JOptionPane;

import model.ActionNode;
import model.ActionType;
import model.DrMercer;
import model.Dylan;
import model.Game;
import model.Item;
import model.ItemType;
import model.MapStrategy;
import model.MapType;
import model.Player;
import model.ScenarioType;
import model.Team;
import model.Tile;
import model.Unit;

public class ConsoleView implements Observer, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Game game;
	private Scanner inputScanner = new Scanner(System.in);
	private String legend;
	private boolean isSelectingATile;
	private boolean isSelectingAnAction;
	private ActionNode recentAction;
	@SuppressWarnings("unused")
	private boolean isOpeningInventory;
	@SuppressWarnings("unused")
	private boolean isOpeningShop;
	@SuppressWarnings("unused")
	private boolean isAttacking;
	@SuppressWarnings("unused")
	private boolean isMoving;
	@SuppressWarnings("unused")
	private boolean isUsingSpecial;
	@SuppressWarnings("unused")
	private Tile tileToActOn;
	@SuppressWarnings("unused")
	private boolean isApplyingAction;

	public ConsoleView() {
		setUpLegend();
		setUpGame();

	}

	public static void main(String[] args) {
		ConsoleView view = new ConsoleView();
		view.start();
		// System.out.println(game.toString());
		// print other useful stuff

	}

	/**
	 * Starts the game
	 */
	private void start() {
		System.out.println(game.toString());
		System.out.println(legend);
		while (!game.isGameOver()) {
			isSelectingATile = true;

			promptForTileSelection();
		}

	}

	/**
	 * Ask the user for a row and col to select and then in the game select the
	 * tile at that row and col
	 */
	private void promptForTileSelection() {
		// TODO Auto-generated method stub
		// This will call a method in game that changes the model and causes
		// this view to update
		int row = -100;
		int col = -100;
		do {
			System.out.println("Type -1 for a new game.");
			System.out.println("Type -2 to end turn");
			System.out.println("Please select a row: ");
			row = inputScanner.nextInt();
			if (row == -2) {
				endTurn();
				break;
			} else if (row == -1) {
				startNewGame();
				break;
			}
			System.out.println("Please select a column: ");
			col = inputScanner.nextInt();
			if (col == -2) {
				endTurn();
				break;
			} else if (col == -1) {
				startNewGame();
				break;
			}
		} while (tileSelectionIsInvalid(row, col));
		if (row != -2 && col != -1 && row != -1 && col != -2) {
			game.selectUnit(row, col);
		}

	}

	private void startNewGame() {
		setUpGame();
		isSelectingATile = true;
		isSelectingAnAction = false;
		isOpeningInventory = false;
		isOpeningShop = false;
		isAttacking = false;
		isMoving = false;
		isUsingSpecial = false;
		recentAction = null;
		tileToActOn = null;
		isSelectingATile = false;
		isApplyingAction = false;
	}

	private void endTurn() {
		game.endTurn();
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return true: if the row or column is out of bounds false: if the row or
	 *         column is in bounds
	 */

	private boolean tileSelectionIsInvalid(int row, int col) {
		return row < 0 || row > MapStrategy.EASY_MAP_SIZE || col < 0
				|| col > MapStrategy.EASY_MAP_SIZE;
	}

	// private void executeUserCommand(String temp) {
	// String command = temp.toUpperCase();
	// switch (command) {
	// case "MOVE":
	// game.applyAction(tile);
	//
	// }
	//
	// }

	/**
	 * Sets up the game
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public void setUpGame() {

		String userInput = "";
		// System.out.println("Select difficulty");
		// System.out.println("Select scenario");
		// System.out.println("Select map");
		Player player = new Player();
		List<Unit> playerUnits = new LinkedList<>();
		playerUnits.add(new Dylan());
		playerUnits.get(0).setTeam(Team.PLAYER);
		playerUnits.add(new DrMercer());
		playerUnits.get(1).setTeam(Team.PLAYER);
		playerUnits.get(0).getInventory().add(new Item(ItemType.HEAL, 0));
		player.setUnits(playerUnits);
		game = game.getInstance();

		game.setPlayer(player);
		game.setScenario(ScenarioType.KILL_COMMANDER);
		game.setMap(MapType.EASY);
		game.addObserver(this);

	}

	/**
	 * Loads the legend from a file
	 */
	public void setUpLegend() {
		legend = "";
		File inputFile = new File("resources/MapLegend.txt");
		Scanner fileScanner;
		try {
			fileScanner = new Scanner(inputFile);
		} catch (FileNotFoundException e) {
			fileScanner = null;
			JOptionPane.showMessageDialog(null, "Failed to load MapLegend.txt");
			System.exit(1);
		}
		while (fileScanner.hasNext()) {
			legend += fileScanner.nextLine();
			legend += "\n";
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println(game.toString());
		System.out.println(legend);
		if (isSelectingATile) {
			isSelectingATile = false;
			List<ActionNode> actions = game.getTileActions();
			if (actions != null) {
				isSelectingAnAction = true;
				promptForActionSelection(actions);
			}
		} else if (isSelectingAnAction) {
			isSelectingAnAction = false;
			if (recentAction.getAction() == ActionType.INVENTORY) {
				isOpeningInventory = true;
				promptForInventorySelection(game.getPlayerInventory());
			} else if (recentAction.getAction() == ActionType.SHOP) {
				isOpeningShop = true;
				promptForShopSelection(game.getStoreItems());
			} else if (recentAction.getAction() == ActionType.ATTACK) {
				isAttacking = true;
				promptForTileApplication(ActionType.ATTACK);
			} else if (recentAction.getAction() == ActionType.MOVE) {
				isMoving = true;
				promptForTileApplication(ActionType.MOVE);
			} else if (recentAction.getAction() == ActionType.SPECIAL) {
				isUsingSpecial = true;
				promptForTileApplication(ActionType.SPECIAL);
			} else {
				System.out.println(game.isInventoryIsOpen());
				game.resetSelection();
			}
		} /*
		 * else if (isOpeningInventory) { isOpeningInventory = false;
		 * promptForTileSelection(); } else if (isOpeningShop) { isOpeningShop =
		 * false; promptForTileSelection(); } else if (isAttacking) {
		 * isAttacking = false; // promptForTileSelection(); } else if
		 * (isMoving) { isMoving = false; //promptForTileSelection(); } else if
		 * (isUsingSpecial) { isUsingSpecial = false;
		 * //promptForTileSelection(); } else if (isApplyingAction) {
		 * isApplyingAction = false; game.applyAction(tileToActOn); }
		 */

	}

	private void promptForInventorySelection(List<Item> playerInventory) {
		if (playerInventory.size() != 0) {
			System.out
					.println("What would you like to do? Enter the number of the item. ");
			int num;
			do {
				for (int i = 1; i < playerInventory.size() + 1; i++) {
					System.out.println(i + ". "
							+ playerInventory.get(i - 1).getName());
				}
				num = inputScanner.nextInt();
				num--;
			} while (listSelectionIsInvalid(num, playerInventory.size()));

			Unit unit = game.getSelectedTile().getCurrentUnit();
			unit.useItem(playerInventory.get(num));
		} else {
			System.out.println("No items to use");
		}

		System.out.println(game.toString());
		System.out.println(legend);
	}

	/**
	 * Gives the user a list of tiles they can apply the most recent action to.
	 * Updates the model with the user's choice
	 * 
	 * @param action
	 *            the most recent type of action
	 */
	private void promptForTileApplication(ActionType action) {
		String actionName;
		switch (action) {
		case ATTACK:
			actionName = "attack";
			break;
		case SPECIAL:
			actionName = "special";
			break;
		case MOVE:
			actionName = "Move";
			break;
		default:
			actionName = "action";
		}
		System.out.println("Select the tile to apply the " + actionName
				+ " to:");

		// Get the tiles from the game
		List<Tile> tiles = game.getTilesToApplyAction();
		int choice;
		do {
			// Itterate over the tiles and print them
			for (int index = 0; index < tiles.size(); index++) {
				System.out.println((index + 1) + ". "
						+ game.getCoordinate(tiles.get(index)));
			}
			// Get the user input
			choice = inputScanner.nextInt();
			choice--;
		} while (listSelectionIsInvalid(choice, tiles.size()));
		// Apply the action to the selected tile
		game.applyAction(tiles.get(choice));
	}

	/**
	 * Will display the list of shop items available and ask the user for a
	 * choice. This updates the model when the user makes a choice
	 * 
	 * @param storeItems
	 */
	private void promptForShopSelection(List<Item> storeItems) {
		// TODO Auto-generated method stub
		System.out
				.println("What would you like to do? Enter the number of the item. ");
		int num;
		do {
			for (int i = 1; i < storeItems.size() + 1; i++) {
				System.out.println(i + ". " + storeItems.get(i - 1).getName());
			}
			num = inputScanner.nextInt();
			num--;
		} while (listSelectionIsInvalid(num, storeItems.size()));

		boolean canBuy = game.getCurrentPlayer().canBuy(
				storeItems.get(num).getPrice());

		if (canBuy) {
			Unit unit = game.getSelectedTile().getCurrentUnit();
			unit.addToInventory(storeItems.get(num));
			game.getCurrentPlayer().buy(storeItems.get(num).getPrice());
		} else {
			System.out.println("NO. I DON'T WANT DAT.");
		}

	}

	/**
	 * Prompt the user to select an action that comes from the list generated by
	 * the game when a tile is selected. This will update the game with the
	 * action that the user chooses. Keeps track of the action that is selected
	 * 
	 * @param actions
	 *            the actions from the game for the currently selected tile
	 */
	private void promptForActionSelection(List<ActionNode> actions) {
		// TODO Auto-generated method stub
		int actionSelected;
		do {
			itterateThroughActions(actions);
			actionSelected = inputScanner.nextInt();
			actionSelected--;
		} while (listSelectionIsInvalid(actionSelected, actions.size()));

		recentAction = actions.get(actionSelected);
		game.selectUnitAction(actions.get(actionSelected).getAction());
	}

	private boolean listSelectionIsInvalid(int actionSelected, int size) {
		return actionSelected < 0 || actionSelected > size;
	}

	private void itterateThroughActions(List<ActionNode> actions) {
		System.out
				.println("What would you like to do? Type the number for the action");
		for (int index = 0; index < actions.size(); index++) {
			System.out.println((index + 1) + ". "
					+ actions.get(index).getActionName());
		}
	}
}
