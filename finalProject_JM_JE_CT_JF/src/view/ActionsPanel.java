/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import model.ActionNode;
import model.ActionType;
import model.Game;
import model.GameState;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This Jpanel will contain the buttons for the actions a unit can
 *         perform
 */
@SuppressWarnings("serial")
public class ActionsPanel extends JPanel {

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 * 
	 *         Sets the action in the game to the action corresonding to the
	 *         sender
	 */
	private class ActionButtonListener implements ActionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			ActionButton sender = (ActionButton) e.getSource();
			if (sender.equals(shopButton)
					&& Game.getInstance().getState() == GameState.SHOPPING) {
				Game.getInstance().closeTheStore();
			} else if (sender.equals(inventoryButton)
					&& (Game.getInstance().getState() == GameState.INVENTORY || Game
							.getInstance().getState() == GameState.SELECTING_ITEMS)) {
				Game.getInstance().closeInventory();
			} else {
				Game.getInstance().selectUnitAction(sender.getActionType());
			}

		}
	}

	ActionButton attackButton;
	ActionButton inventoryButton;
	ActionButton moveButton;
	ActionButton nothingButton;
	ActionButton tradeButton;
	ActionButton shopButton;
	ActionButton specialButton;

	List<ActionButton> buttons;

	/**
	 * In this consutrctor start this panel off with the elements for the
	 * navigation panel
	 */
	public ActionsPanel() {
		super();
		setPreferredSize(new Dimension(800, 50));
		buttons = new ArrayList<>();
		initializeComponents();
		this.setLayout(new GridLayout(1, 7));
		addComponetsToList();
		addActionListeners();
		addComponentsToPanel();
	}

	/**
	 * Adds the components to the list
	 */
	private void addComponetsToList() {
		buttons.add(attackButton);
		buttons.add(inventoryButton);
		buttons.add(moveButton);
		buttons.add(nothingButton);
		buttons.add(tradeButton);
		buttons.add(shopButton);
		buttons.add(specialButton);

	}

	/**
	 * Adds the actionlisteners to the buttons
	 */
	private void addActionListeners() {
		for (ActionButton b : buttons) {
			b.addActionListener(new ActionButtonListener());
		}

	}

	/**
	 * 
	 */
	private void addComponentsToPanel() {
		this.add(attackButton);
		this.add(inventoryButton);
		this.add(moveButton);
		this.add(nothingButton);
		this.add(tradeButton);
		this.add(shopButton);
		this.add(specialButton);
	}

	/**
	 * 
	 */
	private void initializeComponents() {
		attackButton = new ActionButton("Attack", ActionType.ATTACK);
		attackButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.ATTACK));
		attackButton.setDisabledIcon(ImageFactory.getInstance()
				.getDisabledImage(ButtonType.ATTACK));
		attackButton.setEnabled(false);
		inventoryButton = new ActionButton("Inventory", ActionType.INVENTORY);
		inventoryButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.INVENTORY));
		inventoryButton.setDisabledIcon(ImageFactory.getInstance()
				.getDisabledImage(ButtonType.INVENTORY));
		inventoryButton.setEnabled(false);
		moveButton = new ActionButton("Move", ActionType.MOVE);
		moveButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.MOVE));
		moveButton.setDisabledIcon(ImageFactory.getInstance().getDisabledImage(
				ButtonType.MOVE));
		moveButton.setEnabled(false);
		nothingButton = new ActionButton("Do Nothing", ActionType.NOTHING);
		nothingButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.DO_NOTHING));
		nothingButton.setDisabledIcon(ImageFactory.getInstance()
				.getDisabledImage(ButtonType.DO_NOTHING));
		nothingButton.setEnabled(false);
		tradeButton = new ActionButton("Trade", ActionType.TRADE);
		tradeButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.TRADE));
		tradeButton.setDisabledIcon(ImageFactory.getInstance()
				.getDisabledImage(ButtonType.TRADE));
		tradeButton.setEnabled(false);
		shopButton = new ActionButton("Shop", ActionType.SHOP);
		shopButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.SHOP));
		shopButton.setDisabledIcon(ImageFactory.getInstance().getDisabledImage(
				ButtonType.SHOP));
		shopButton.setEnabled(false);
		specialButton = new ActionButton("Special", ActionType.SPECIAL);
		specialButton.setIcon(ImageFactory.getInstance().getEnabledImage(
				ButtonType.SPECIAL));
		specialButton.setDisabledIcon(ImageFactory.getInstance()
				.getDisabledImage(ButtonType.SPECIAL));
		specialButton.setEnabled(false);
	}

	/**
	 * This method will refresh the state of the buttons. It will go through the
	 * list of actions and for any button that is supposed to be enabled it will
	 * enable that button for any button that is supposed to be disabled it will
	 * disable that button
	 * 
	 * @param actions
	 *            the list of actionnodes for the selected unit
	 */
	public void refreshButtons(List<ActionNode> actions) {
		for (ActionNode a : actions) {
			for (ActionButton b : buttons) {
				if (b.getActionType() == a.getAction()) {
					b.setEnabled(!a.isDisabled());
				}
			}
		}
	}

	public void disableButtons() {
		for (ActionButton b : buttons) {
			b.setEnabled(false);
		}
	}

	public void setSpecialText(String specialText) {
		specialButton.setToolTipText(specialText);
		revalidate();
		repaint();
	}

	public void enableTheShop() {
		shopButton.setEnabled(true);

	}

	public void setShopText(String text) {
		shopButton.setToolTipText(text);
	}

	public void enableTheInventory() {
		inventoryButton.setEnabled(true);

	}

	public void setInventoryText(String text) {
		inventoryButton.setToolTipText(text);

	}

}
