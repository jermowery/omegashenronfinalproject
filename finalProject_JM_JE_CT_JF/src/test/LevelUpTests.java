/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;
import model.ExperienceStrategy;
import model.RoidRageDiver;
import model.StatStrategy;
import model.StatType;
import model.Unit;
import model.Weaver;

import org.junit.Test;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class LevelUpTests {

	@Test
	public void test() {
		Unit myUnit1 = new Weaver();
		Unit myUnit2 = new RoidRageDiver();
		assertEquals(myUnit2.getStat(StatType.HP),
				StatStrategy.ROID_RAGE_DIVER_HP);
		assertEquals(myUnit2.getStat(StatType.ATTACK),
				StatStrategy.ROID_RAGE_DIVER_ATTACK);
		assertEquals(myUnit2.getStat(StatType.DEFENSE),
				StatStrategy.ROID_RAGE_DIVER_DEFENSE);
		ExperienceStrategy.calculateLevel(myUnit2, myUnit1, 100);
		assertEquals(myUnit2.getStat(StatType.HP),
				StatStrategy.ROID_RAGE_DIVER_HP + 2);
		assertEquals(myUnit2.getStat(StatType.ATTACK),
				StatStrategy.ROID_RAGE_DIVER_ATTACK + 2);
		assertEquals(myUnit2.getStat(StatType.DEFENSE),
				StatStrategy.ROID_RAGE_DIVER_DEFENSE + 2);
		ExperienceStrategy.calculateLevel(myUnit2, myUnit1, 100);
		assertEquals(myUnit2.getStat(StatType.HP),
				StatStrategy.ROID_RAGE_DIVER_HP + 4);
		assertEquals(myUnit2.getStat(StatType.ATTACK),
				StatStrategy.ROID_RAGE_DIVER_ATTACK + 4);
		assertEquals(myUnit2.getStat(StatType.DEFENSE),
				StatStrategy.ROID_RAGE_DIVER_DEFENSE + 4);
	}

}
