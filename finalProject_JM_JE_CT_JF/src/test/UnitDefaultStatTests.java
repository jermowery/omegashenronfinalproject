/**
 * 
 */
package test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import model.ActionNode;
import model.ActionType;
import model.AnnoyingBrat;
import model.Commander;
import model.DrMercer;
import model.Dylan;
import model.Gearhead;
import model.RoidRageDiver;
import model.StatNode;
import model.Tesla;
import model.Trooper;
import model.Unit;
import model.Weaver;

import org.junit.Test;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class UnitDefaultStatTests {

	@Test
	public void testWeaverStats() {
		Unit myWeaver = new Weaver();
		List<StatNode> stats = myWeaver.getStatList();
		List<ActionNode> actions = myWeaver.getActions();
		System.out.println("Weaver: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}
		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(5, stats.get(1).getStatModification());
		assertEquals(15, stats.get(2).getStatModification());
		assertEquals(10, stats.get(3).getStatModification());
		assertEquals(4, stats.get(4).getStatModification());
		assertEquals(7, stats.get(5).getStatModification());
		assertEquals(3, stats.get(6).getStatModification());
		assertEquals(1, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		actions = myWeaver.getActions();

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new Trooper();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Trooper: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(15, stats.get(1).getStatModification());
		assertEquals(30, stats.get(2).getStatModification());
		assertEquals(15, stats.get(3).getStatModification());
		assertEquals(5, stats.get(4).getStatModification());
		assertEquals(1, stats.get(5).getStatModification());
		assertEquals(2, stats.get(6).getStatModification());
		assertEquals(2, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new AnnoyingBrat();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Annoying Brat: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(5, stats.get(1).getStatModification());
		assertEquals(10, stats.get(2).getStatModification());
		assertEquals(5, stats.get(3).getStatModification());
		assertEquals(7, stats.get(4).getStatModification());
		assertEquals(1, stats.get(5).getStatModification());
		assertEquals(2, stats.get(6).getStatModification());
		assertEquals(2, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new RoidRageDiver();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Roid Rage Diver: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(50, stats.get(1).getStatModification());
		assertEquals(40, stats.get(2).getStatModification());
		assertEquals(50, stats.get(3).getStatModification());
		assertEquals(3, stats.get(4).getStatModification());
		assertEquals(1, stats.get(5).getStatModification());
		assertEquals(1, stats.get(6).getStatModification());
		assertEquals(1, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new DrMercer();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("DR MERCER: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(9001, stats.get(1).getStatModification());
		assertEquals(1000, stats.get(2).getStatModification());
		assertEquals(1000, stats.get(3).getStatModification());
		assertEquals(100, stats.get(4).getStatModification());
		assertEquals(1000, stats.get(5).getStatModification());
		assertEquals(1001, stats.get(6).getStatModification());
		assertEquals(1, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new Dylan();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Dylan: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(9001, stats.get(1).getStatModification());
		assertEquals(1000, stats.get(2).getStatModification());
		assertEquals(1000, stats.get(3).getStatModification());
		assertEquals(100, stats.get(4).getStatModification());
		assertEquals(1000, stats.get(5).getStatModification());
		assertEquals(1001, stats.get(6).getStatModification());
		assertEquals(1, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new Gearhead();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Gearhead: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(20, stats.get(1).getStatModification());
		assertEquals(20, stats.get(2).getStatModification());
		assertEquals(10, stats.get(3).getStatModification());
		assertEquals(7, stats.get(4).getStatModification());
		assertEquals(1, stats.get(5).getStatModification());
		assertEquals(2, stats.get(6).getStatModification());
		assertEquals(2, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new Trooper();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Trooper: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(15, stats.get(1).getStatModification());
		assertEquals(30, stats.get(2).getStatModification());
		assertEquals(15, stats.get(3).getStatModification());
		assertEquals(5, stats.get(4).getStatModification());
		assertEquals(1, stats.get(5).getStatModification());
		assertEquals(2, stats.get(6).getStatModification());
		assertEquals(2, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = new Tesla();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Tesla: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		assertEquals(0, stats.get(0).getStatModification());
		assertEquals(15, stats.get(1).getStatModification());
		assertEquals(15, stats.get(2).getStatModification());
		assertEquals(7, stats.get(3).getStatModification());
		assertEquals(4, stats.get(4).getStatModification());
		assertEquals(7, stats.get(5).getStatModification());
		assertEquals(2, stats.get(6).getStatModification());
		assertEquals(2, stats.get(7).getStatModification());

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);

		System.out.println();
		System.out.println();

		myWeaver = Commander.getInstance();
		stats = myWeaver.getStatList();
		actions = myWeaver.getActions();
		System.out.println("Commander: ");
		System.out.println();
		// Show the printing is correct
		for (int index = 0; index < stats.size(); index++) {
			System.out.println(stats.get(index).getStatNameType() + ": "
					+ stats.get(index).getStatModification());
		}

		assertEquals("Experience", stats.get(0).getStatNameType());
		assertEquals("Attack", stats.get(1).getStatNameType());
		assertEquals("HP", stats.get(2).getStatNameType());
		assertEquals("Defense", stats.get(3).getStatNameType());
		assertEquals("Movement", stats.get(4).getStatNameType());
		assertEquals("Attack Range", stats.get(5).getStatNameType());
		assertEquals("Action Points", stats.get(6).getStatNameType());
		assertEquals("Special Action Usage", stats.get(7).getStatNameType());

		
		

		assertEquals(actions.get(0).getAction(), ActionType.MOVE);
		assertEquals(actions.get(1).getAction(), ActionType.ATTACK);
		assertEquals(actions.get(2).getAction(), ActionType.NOTHING);
		assertEquals(actions.get(3).getAction(), ActionType.SHOP);
		assertEquals(actions.get(4).getAction(), ActionType.INVENTORY);
		assertEquals(actions.get(5).getAction(), ActionType.TRADE);
	}

}
