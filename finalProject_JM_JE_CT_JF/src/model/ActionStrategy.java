/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class ActionStrategy implements Serializable {

	public static final String WEAVER_SPECIAL = "Chamois";
	public static final int WEAVER_ACTION_POINTS = 1;
	public static final String TROOPER_SPECIAL = "Demoarlize";
	public static final int TROOPER_ACTION_POINTS = 2;
	public static final String TESLA_SPECIAL = "Tesla Coil";
	public static final int TESLA_ACTION_POINTS = 2;
	public static final String ROID_RAGE_DIVER_SPECIAL = "Diver SMASH!";
	public static final int ROID_RAGE_DIVER_POINTS = 1;
	public static final String GEARDHEAD_SPECIAL = "Sanguis";
	public static final int GEARHEARD_ACTION_POINTS = 2;
	public static final String DYLAN_SPECIAL = "CINDEEZLE!";
	public static final int DYLAN_ACTION_POINTS = 1;
	public static final String DRMERCER_SPECIAL = "@Test";
	public static final int DRMERCER_POINTS = 1;
	public static final String ANNOYING_BRAT_SPECIAL = "Recover";
	public static final int ANNOYING_BRAT_ACTION_POINTS = 2;
	public static final int MOVEMENT_POINTS = 1;
	public static final int ATTACK_POINTS = 1;
	public static final int NOTHING_POINTS = 0;
	public static final int SHOP_POINTS = 0;
	public static final String ERIC_SPECIAL = "Forget the donuts";
	public static final int ERIC_ACTION_POINTS = 1;
	public static final int INVENTORY_POINTS = 0;
	public static final String COMMANDER_SPECIAL = "Shrug";
	public static final int COMMANDER_ACTION_POINTS = 1;
	public static final int TRADE_POINTS = 0;

	public static String getSpecialAsString(UnitType type) {
		switch (type) {
		case ANNOYINGBRAT:
			return ANNOYING_BRAT_SPECIAL;
		case COMMANDER:
			return COMMANDER_SPECIAL;
		case DRMERCER:
			return DRMERCER_SPECIAL;
		case DYLAN:
			return DYLAN_SPECIAL;
		case ERIC:
			return ERIC_SPECIAL;
		case GEARHEAD:
			return GEARDHEAD_SPECIAL;
		case ROIDRAGEDIVER:
			return ROID_RAGE_DIVER_SPECIAL;
		case TESLA:
			return TESLA_SPECIAL;
		case TROOPER:
			return TROOPER_SPECIAL;
		case WEAVER:
			return WEAVER_SPECIAL;
		default:
			return null;

		}
	}

}
