/**
 * 
 */
package model;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class CheatStrategy {

	public static final String ERIC_STRING = "i forgot";
	public static final String DYLAN_STRING = "you have to know where to tickle me";
	public static final String MERCER_STRING = "always write the tests first";

}
