package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Trooper extends Unit implements Serializable {

	public Trooper() {
		super();
		setType(UnitType.TROOPER);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.TROOPER));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.TROOPER));
		super.setStatList(stats);
	}
}
