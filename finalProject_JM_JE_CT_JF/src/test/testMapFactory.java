package test;

import model.MapFactory;

import org.junit.Test;

public class testMapFactory {

	
	@SuppressWarnings("static-access")
	@Test
	public void testGetTestMap(){
		MapFactory testFactory = new MapFactory();
		testFactory.getEasyMap();
		testFactory.getHardMap();
		testFactory.getMediumMap();
		testFactory.getRandomMap();
		
		testFactory.getTestMap();
	}
}
