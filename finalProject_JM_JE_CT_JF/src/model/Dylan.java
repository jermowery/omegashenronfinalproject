package model;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: the special Dylan unit only accessible by cheats
 * @Authors: Cindy Trieu, Jeremy Mowery, Jamel El-Merhaby, Jake Fritts
 */
@SuppressWarnings("serial")
public class Dylan extends Unit implements Serializable {
	public Dylan() {
		super();
		setType(UnitType.DYLAN);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.DYLAN));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.DYLAN));
		super.setStatList(stats);
	}
}
