/**
 * 
 */
package model;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class MoneyStrategy {
	private static final int VICTORY_MONEY = 25;

	public static int calculateMoneyGain(Unit defeatedUnit) {
		return defeatedUnit.getStat(StatType.ATTACK)
				+ defeatedUnit.getStat(StatType.DEFENSE)
				+ defeatedUnit.getStat(StatType.ATTACK_RANGE)
				+ defeatedUnit.getStat(StatType.MOVEMENT) + VICTORY_MONEY;
	}
}
