package view;

public enum ButtonType {
	ATTACK, DO_NOTHING, SHOP, INVENTORY, TRADE, SPECIAL, MOVE;
}
