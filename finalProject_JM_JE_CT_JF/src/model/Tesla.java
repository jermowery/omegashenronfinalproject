package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Tesla extends Unit implements Serializable {
	public Tesla() {
		super();
		setType(UnitType.TESLA);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.TESLA));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.TESLA));
		super.setStatList(stats);
	}
}
