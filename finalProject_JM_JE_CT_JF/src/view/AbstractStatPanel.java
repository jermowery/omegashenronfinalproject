/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.StatNode;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public abstract class AbstractStatPanel extends JPanel {

	private BufferedImage sprite;
	private JLabel nameLabel;
	private List<StatNode> stats;
	private List<DoubleLabel> labels;

	/**
	 * 
	 */
	public AbstractStatPanel() {
		stats = new ArrayList<>();
		labels = new ArrayList<>();
	}

	/**
	 * @param layout
	 */
	public AbstractStatPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public AbstractStatPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public AbstractStatPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	public void updateStats(List<StatNode> stats) {
		this.removeAll();
		TileImagePanel imagePanel = new TileImagePanel(sprite);
		imagePanel.setSize(new Dimension(ViewStrategy.TILE_HEIGHT,
				ViewStrategy.TILE_WIDTH));

		this.add(imagePanel);
		this.add(nameLabel);
		this.stats = new ArrayList<>();
		this.labels = new ArrayList<>();
		setStats(stats);
		for (StatNode s : stats) {
			DoubleLabel temp = new DoubleLabel(s.getStatType(),
					s.getStatModification());
			labels.add(temp);
		}
		for (DoubleLabel d : labels) {
			this.add(d.getStatName());
			this.add(d.getStatValue());
		}
		// validate();
		// Graphics2D g2 = (Graphics2D) dummy.getGraphics();
		// g2.drawImage(sprite, 0, 0, ViewStrategy.TILE_WIDTH,
		// ViewStrategy.TILE_HEIGHT, null);
		validate();
		// dummy.repaint();
		// this.repaint();
	}

	public BufferedImage getSprite() {
		return sprite;
	}

	public void setSprite(BufferedImage sprite) {
		this.sprite = sprite;
	}

	public List<StatNode> getStats() {
		return stats;
	}

	public void setStats(List<StatNode> stats) {
		this.stats = stats;
	}

	public JLabel getNameLabel() {
		return nameLabel;
	}

	public void setNameLabel(JLabel nameLabel) {
		this.nameLabel = nameLabel;
	}

	public void removeStats() {
		this.removeAll();
		this.repaint();

	}

}
