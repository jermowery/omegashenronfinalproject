package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MapStrategy implements Serializable {

	public static final int EASY_MAP_SIZE = 20;
	public static final int MEDIUM_MAP_SIZE = 25;
	public static final int HARD_MAP_SIZE = 30;
	public static final int RANDOM_MAP_SIZE = 30;
}
