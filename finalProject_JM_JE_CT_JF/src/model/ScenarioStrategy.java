/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class ScenarioStrategy implements Serializable {

	public static final int ENEMIES_REMAINING = 0;
	private static final int DRAGONBALLS_PRICE = 10000;
	public static final Item OBJECTIVE_ITEM = new Item(ItemType.DRAGONBALLS,
			DRAGONBALLS_PRICE);
	public static final Unit OBJECTIVE_UNIT = Commander.getInstance();
	public static final int MAP_COL = 0;

	public static String getScenarioAsString(ScenarioType type) {
		String toReturn = "";

		switch (type) {
		case BUY_DRAGONBALLS:
			toReturn += "Collect all seven magical Dragon Balls.";
			break;
		case KILL_COMMANDER:
			toReturn += "Assassinate the enemy commander!";
			break;
		default:
			toReturn += "Reach the safe zone before being slaughtered like stupid pigs!!!";
			break;
		}

		return toReturn;
	}

}
