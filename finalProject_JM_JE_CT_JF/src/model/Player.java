package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description: a class to represent a player and a list of units the player
 *               selected pre-game.
 * @Authors: Cindy Trieu, Jeremy Mowery, Jamel El-Merhaby, Jake Fritts
 */
public class Player implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The players money
	 */
	protected int money;
	/**
	 * The list of units controlled by the player
	 */
	protected List<Unit> unitList;
	protected Inventory inventory;

	public Player() {
		money = PlayerStrategy.STARTING_WEALTH;
		inventory = new Inventory();
		unitList = new LinkedList<>();
	}

	/**
	 * Determines whether the player can afford to purchase an item.
	 * 
	 * @param price
	 * @return true if the user has enough money to make the transaction, false
	 *         otherwise.
	 */
	public boolean canBuy(int price) {
		return getMoney() >= price;

	}

	/**
	 * 
	 * @return The amount of money that the player has.
	 */
	public int getMoney() {
		// TODO Auto-generated method stub
		return money;
	}

	public void buy(int price) {
		setMoney(getMoney() - price);
		// we need to add the item or unit to the players list

	}

	/**
	 * Updates the users money with a new amount.
	 * 
	 * @param newMoney
	 */
	private void setMoney(int newMoney) {
		this.money = newMoney;

	}

	public List<Item> getInventory() {

		return inventory.getAllItems();
	}

	public List<Unit> getUnits() {
		return unitList;
	}

	public void setUnits(List<Unit> playerUnits) {
		this.unitList = playerUnits;

	}

	public void addToMoney(int money) {
		this.money += money;

	}

}
