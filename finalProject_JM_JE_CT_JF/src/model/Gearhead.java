package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Gearhead extends Unit implements Serializable {
	public Gearhead() {
		super();
		setType(UnitType.GEARHEAD);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.GEARHEAD));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.GEARHEAD));
		super.setStatList(stats);
	}
}
