/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public enum UnitType implements Serializable {
	WEAVER, TROOPER, TESLA, GEARHEAD, DYLAN, DRMERCER, ANNOYINGBRAT, ROIDRAGEDIVER, ERIC, COMMANDER;

}
