package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class AnnoyingBrat extends Unit implements Serializable {
	public AnnoyingBrat() {
		super();
		setType(UnitType.ANNOYINGBRAT);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.ANNOYINGBRAT));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.ANNOYINGBRAT));
		super.setStatList(stats);
	}
}
