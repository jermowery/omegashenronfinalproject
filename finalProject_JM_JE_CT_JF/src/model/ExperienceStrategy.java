package model;

/**
 * 
 */

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class ExperienceStrategy {

	/**
	 * Ammount to increment stats by on level up
	 */
	public static final int LEVEL_UP_INCREMENT = 2;
	/**
	 * The experience required to level up
	 */
	public static final int MAX_EXPERIENCE = 100;

	/**
	 * Calculates the level for the attacking unit based on the stats of the
	 * dending unit and the damage done
	 * 
	 * @param attackingUnit
	 * @param defendingUnit
	 * @param damage
	 */
	public static void calculateLevel(Unit attackingUnit, Unit defendingUnit,
			int damage) {
		int sumOfStats = sumStats(defendingUnit);
		int experience = damage + sumOfStats / 4;
		while (needsToLevelUp(defendingUnit, experience)) {
			int experienceNedded = MAX_EXPERIENCE
					- attackingUnit.getStat(StatType.EXPERIENCE);
			experience -= experienceNedded;
			levelUp(attackingUnit, experience);
		}
		// Add the remaining experience to the unit's experience
		attackingUnit.setStat(StatType.EXPERIENCE, experience);
	}

	/**
	 * Levels up the unit
	 * 
	 * @param attackingUnit
	 * @param experience
	 */
	private static void levelUp(Unit attackingUnit, int experience) {
		// Determine how much experience we need to subtract

		adjustStats(attackingUnit);
	}

	private static void adjustStats(Unit attackingUnit) {
		// Get the list of stats and incremement, attack, defense, and HP by 2.
		attackingUnit.setStat(StatType.ATTACK,
				attackingUnit.getStat(StatType.ATTACK) + LEVEL_UP_INCREMENT);
		attackingUnit.setStat(StatType.DEFENSE,
				attackingUnit.getStat(StatType.DEFENSE) + LEVEL_UP_INCREMENT);
		attackingUnit.setStat(StatType.HP, attackingUnit.getStat(StatType.HP)
				+ LEVEL_UP_INCREMENT);
		// Set the experience to the default
		attackingUnit.setStat(StatType.EXPERIENCE,
				StatStrategy.DEFAULT_EXPERIENCE);
	}

	/**
	 * @param defendingUnit
	 * @param experience
	 * @return
	 */
	public static boolean needsToLevelUp(Unit defendingUnit, int experience) {
		return defendingUnit.getStat(StatType.EXPERIENCE) + experience >= MAX_EXPERIENCE;
	}

	private static int sumStats(Unit defendingUnit) {
		return defendingUnit.getStat(StatType.ATTACK)
				+ defendingUnit.getStat(StatType.DEFENSE);
	}
}
