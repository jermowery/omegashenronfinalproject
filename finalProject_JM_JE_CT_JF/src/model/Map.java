/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class Map implements Serializable {
	private Tile[][] map;
	private List<Unit> player;
	private List<Unit> enemy;
	private Unit selectedUnit = null;
	private ActionType actionType;

	/**
	 * a map constructor that allows units to be placed on the map for testing
	 * purposes.
	 * 
	 */

	public Map() {

		map = MapFactory.getTestMap();

	}

	/**
	 * sets a unit in the passed location for testing purposes.
	 */
	public void placeUnit(Unit unit, int row, int col) {
		map[row][col].setCurrentUnit(unit);
	}

	/**
	 * takes a MapType enum and calls static methods from the MapFactory class
	 * to populate the array correctly
	 * 
	 * @param type
	 *            the type of the map to generate
	 */
	public Map(MapType type, List<Unit> playerIn, List<Unit> enemyIn) {
		player = new ArrayList<>();
		enemy = new ArrayList<>();

		player.addAll(playerIn);
		enemy.addAll(enemyIn);

		if (type.equals(MapType.TEST)) {
			map = MapFactory.getTestMap();
		} else if (type.equals(MapType.EASY)) {
			map = MapFactory.getEasyMap();
		} else if (type.equals(MapType.MEDIUM)) {
			map = MapFactory.getMediumMap();
		} else if (type.equals(MapType.HARD)) {
			map = MapFactory.getHardMap();
		} else if (type.equals(MapType.RANDOM)) {
			map = MapFactory.getRandomMap();
		}

		placePlayerUnits(type);
		placeEnemyUnits(type);

	}

	/**
	 * takes a MapType from the constructor to determine how to place player
	 * units
	 * 
	 * @param type
	 *            the type of the generated map
	 */
	private void placePlayerUnits(MapType type) {
		Random rand = new Random();

		while (!player.isEmpty()) {
			int placedRow = 0;
			int placedColumn = 0;

			if (type.equals(MapType.TEST)) {
				placedRow = rand.nextInt(MapStrategy.EASY_MAP_SIZE);
			} else if (type.equals(MapType.EASY)) {
				placedRow = rand.nextInt(MapStrategy.EASY_MAP_SIZE);
			} else if (type.equals(MapType.MEDIUM)) {
				placedRow = rand.nextInt(MapStrategy.MEDIUM_MAP_SIZE);
			} else if (type.equals(MapType.HARD)) {
				placedRow = rand.nextInt(MapStrategy.HARD_MAP_SIZE);
			} else if (type.equals(MapType.RANDOM)) {
				placedRow = rand.nextInt(MapStrategy.RANDOM_MAP_SIZE);
			}

			/**
			 * always places player units in one of the first 5 columns
			 */
			placedColumn = rand.nextInt(6);

			/**
			 * to catch the case a unit would be placed on another unit
			 */
			if (map[placedRow][placedColumn].getCurrentUnit() == null)
				map[placedRow][placedColumn].setCurrentUnit(player.remove(0));

		}
	}

	private void placeEnemyUnits(MapType type) {
		Random rand = new Random();

		while (!enemy.isEmpty()) {
			int placedRow = 0;
			int placedColumn = 0;

			/**
			 * always places enemy units in one of the last five columns based
			 * on the map size
			 */
			if (type.equals(MapType.TEST)) {
				placedRow = rand.nextInt(MapStrategy.EASY_MAP_SIZE);
				placedColumn = rand.nextInt(6)
						+ (MapStrategy.EASY_MAP_SIZE - 6);
			} else if (type.equals(MapType.EASY)) {
				placedRow = rand.nextInt(MapStrategy.EASY_MAP_SIZE);
				placedColumn = rand.nextInt(6)
						+ (MapStrategy.EASY_MAP_SIZE - 6);
			} else if (type.equals(MapType.MEDIUM)) {
				placedRow = rand.nextInt(MapStrategy.MEDIUM_MAP_SIZE);
				placedColumn = rand.nextInt(6)
						+ (MapStrategy.MEDIUM_MAP_SIZE - 6);
			} else if (type.equals(MapType.HARD)) {
				placedRow = rand.nextInt(MapStrategy.HARD_MAP_SIZE);
				placedColumn = rand.nextInt(6)
						+ (MapStrategy.HARD_MAP_SIZE - 6);
			} else if (type.equals(MapType.RANDOM)) {
				placedRow = rand.nextInt(MapStrategy.RANDOM_MAP_SIZE);
				placedColumn = rand.nextInt(6)
						+ (MapStrategy.RANDOM_MAP_SIZE - 6);
			}

			/**
			 * to catch the case a unit would be placed on another unit
			 */
			if (map[placedRow][placedColumn].getCurrentUnit() == null)
				map[placedRow][placedColumn].setCurrentUnit(enemy.remove(0));

		}

	}

	/**
	 * gets all tiles in the attack range, regardless of if there's a unit or
	 * not. used for special abilities that use un-occupied tiles.
	 * 
	 * @param selectedUnit
	 * @return List of Tiles
	 */
	public List<Tile> getTilesInRange(Unit selectedUnit) {
		this.selectedUnit = selectedUnit;
		actionType = ActionType.SPECIAL;
		int range = selectedUnit.getStat(StatType.ATTACK_RANGE);
		Coordinate cord = getUnitCoordinate(selectedUnit);

		List<Tile> tilesInRange = new ArrayList<>();

		for (int row = -range; row < range + 1; row++) {
			for (int col = -range; col < range + 1; col++) {
				/**
				 * checking to see if it's within the bounds
				 */
				if ((cord.getRow() + row) < map.length
						&& (cord.getRow() + row) >= 0
						&& (cord.getColumn() + col) < map[0].length
						&& (cord.getColumn() + col) >= 0) {

					tilesInRange.add(map[cord.getRow() + row][cord.getColumn()
							+ col]);

				}

			}
		}

		return tilesInRange;

	}

	/**
	 * gets all the units, friendly and enemy, in the selectedUnits attack range
	 * 
	 * @param selectedUnit
	 * @return List of Tiles
	 */
	public List<Tile> getUnitsInRange(Unit selectedUnit) {
		this.selectedUnit = selectedUnit;
		int range = 0;
		Coordinate cord;
		@SuppressWarnings("unused")
		int selectedUnitRow = 0;
		@SuppressWarnings("unused")
		int selectedUnitColumn = 0;
		List<Tile> unitsInRange = new ArrayList<>();

		/**
		 * gets the attack range
		 */

		range = selectedUnit.getStat(StatType.ATTACK_RANGE);
		cord = getUnitCoordinate(selectedUnit);

		/**
		 * checks around the unit, no wrap around, based on the unit's range and
		 * adds found enemy units to a list of possible units to attack
		 */

		for (int row = -range; row < range + 1; row++) {
			for (int col = -range; col < range + 1; col++) {
				/**
				 * checking to see if it's within the bounds
				 */
				if ((cord.getRow() + row) < map.length
						&& (cord.getRow() + row) >= 0
						&& (cord.getColumn() + col) < map[0].length
						&& (cord.getColumn() + col) >= 0) {

					/**
					 * nested if's to avoid errors and null pointers. checking
					 * to make sure if the row has a unit
					 */
					if (map[cord.getRow() + row][cord.getColumn() + col]
							.getCurrentUnit() != null)
						unitsInRange.add(map[cord.getRow() + row][cord
								.getColumn() + col]);

				}

			}
		}

		return unitsInRange;
	}

	/**
	 * coordinate is used to find the location on the game board for checking
	 * attack ranges
	 * 
	 * @param unit
	 * @return
	 */
	public Coordinate getUnitCoordinate(Unit unit) {
		Coordinate cord = new Coordinate();
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[0].length; col++) {
				if (map[row][col].getCurrentUnit() != null
						&& map[row][col].getCurrentUnit().equals(unit)) {
					cord.setRow(row);
					cord.setColumn(col);
				}
			}
		}
		return cord;
	}

	/**
	 * used to get the tile, instead of the unit, for special attack
	 * implementation.
	 */
	public Coordinate getTileCoordinate(Tile tile) {
		Coordinate cord = new Coordinate();
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[0].length; col++) {
				if (map[row][col].equals(tile)) {
					cord.setRow(row);
					cord.setColumn(col);
				}
			}
		}
		return cord;
	}

	/**
	 * getting the tile from the passed row and column
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Tile getTile(int row, int col) {
		return map[row][col];
	}

	/**
	 * setting the tile on the map to a new passed tile, used for traps and
	 * breaking walls on the map
	 * 
	 * @param row
	 * @param col
	 * @param tile
	 */
	public void setTile(int row, int col, Tile tile) {
		map[row][col] = tile;
	}

	/**
	 * a list of possible squares to move to based on current movement.
	 * 
	 * @param selectedUnit
	 * @return
	 */
	public List<Tile> getPossibleMovementTiles(Unit selectedUnit) {
		this.selectedUnit = selectedUnit;
		int movement = 0;
		@SuppressWarnings("unused")
		int selectedUnitRow = 0;
		@SuppressWarnings("unused")
		int selectedUnitColumn = 0;
		Coordinate cord;
		List<Tile> tilesInRange = new ArrayList<>();

		/**
		 * sets the movement from the unit
		 */

		movement = selectedUnit.getStat(StatType.MOVEMENT);
		cord = getUnitCoordinate(selectedUnit);

		/**
		 * checks around the unit, no wrap around, based on the unit's movement
		 * and adds possible tiles to a list of tiles
		 */

		for (int row = -movement; row < movement + 1; row++) {
			for (int col = -movement; col < movement + 1; col++) {
				/**
				 * checking to see if it's within the bounds
				 */
				if ((cord.getRow() + row) < map.length
						&& (cord.getRow() + row) >= 0
						&& (cord.getColumn() + col) < map[0].length
						&& (cord.getColumn() + col) >= 0) {

					/**
					 * nested if's to avoid errors and null pointers. checking
					 * to make sure if the row has a unit
					 */
					if (map[cord.getRow() + row][cord.getColumn() + col]
							.getCurrentUnit() == null
							&& !map[cord.getRow() + row][cord.getColumn() + col]
									.getTileType().equals(TileType.WALL))
						tilesInRange.add(map[cord.getRow() + row][cord
								.getColumn() + col]);

				}

			}
		}

		return tilesInRange;
	}

	/**
	 * moves the passed unit to the selected row and column and sets the
	 * previous units location to null;
	 * 
	 * @param unit
	 * @param row
	 * @param column
	 */
	public void moveUnit(Unit unit, int row, int column) {
		// Perform error checking
		row = makeRowValid(row);
		column = makeColumnValid(column);
		Coordinate cord = getUnitCoordinate(unit);
		map[cord.getRow()][cord.getColumn()].setCurrentUnit(null);
		map[row][column].setCurrentUnit(unit);

	}

	private int makeColumnValid(int column) {
		if (column >= map[0].length) {
			return map[0].length - 1;
		} else if (column < 0) {
			return 0;
		}
		return column;

	}

	private int makeRowValid(int row) {
		if (row >= map.length) {
			return map.length - 1;
		} else if (row < 0) {
			return 0;
		}
		return row;

	}

	/**
	 * checks map for the unit that has been destroyed, and removes it from the
	 * grid
	 */
	public void removeUnit(Unit unit) {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[0].length; col++) {
				if (map[row][col].getCurrentUnit() != null
						&& map[row][col].getCurrentUnit().equals(unit)) {
					map[row][col].setCurrentUnit(null);
				}
			}
		}
	}

	/**
	 * a basic two string of the board
	 */
	public String toString() {

		String result = "";
		for (int column = 0; column < map.length; column++) {
			if (column < 10) {
				result += "    0" + column + " ";
			} else {
				result += "    " + column + " ";
			}
		}
		result += "\n";

		// Since this is ridiculous to try to debug I will print out each
		// variable to see if it is null
		// System.out.println(map);
		for (int i = 0; i < map.length; i++) {
			if (i < 10) {
				result += 0;
			}
			result += i;
			for (int j = 0; j < map[0].length; j++) {

				result += "[";

				if (selectedUnit != null) {
					switch (actionType) {
					case ATTACK:
						if (map[i][j].getCurrentUnit() != null
								&& getUnitsInRange(selectedUnit).contains(
										map[i][j]))
							result += "=";
						else
							result += " ";
						break;
					case SPECIAL:
						if (getTilesInRange(selectedUnit).contains(map[i][j]))
							result += "=";
						else
							result += " ";
						break;
					case MOVE:
						if (getPossibleMovementTiles(selectedUnit).contains(
								map[i][j]))
							result += "=";
						else
							result += " ";
						break;
					default:
						break;
					}
				} else
					result += "  ";

				if (map[i][j].getCurrentUnit() != null) {
					Team team = map[i][j].getCurrentUnit().getTeam();
					UnitType type = map[i][j].getCurrentUnit().getType();
					switch (team) {
					case AI:
						result += "!";
						break;
					case PLAYER:
						result += " ";
						break;
					}

					switch (type) {
					case ROIDRAGEDIVER:
						result += "R";
						break;
					case WEAVER:
						result += "W";
						break;
					case TROOPER:
						result += "T";
						break;
					case TESLA:
						result += "S";
						break;
					case ANNOYINGBRAT:
						result += "A";
						break;
					case GEARHEAD:
						result += "G";
						break;
					case DYLAN:
						result += "D";
						break;
					case DRMERCER:
						result += "M";
						break;
					case ERIC:
						result += "E";
						break;
					case COMMANDER:
						result += "C";
						break;
					default:
						System.out
								.println("ERROR, NOT A VALID TYPE IN ALTERING THE LIST OF APPLIED ACTIONS");
						break;

					}

				} else if (map[i][j].getCurrentUnit() == null) {
					TileType tileType = map[i][j].getTileType();

					switch (tileType) {
					case DESERT:
						result += "  ";
						break;
					case RUBBLE:
						result += " *";
						break;
					case WALL:
						result += " /";
						break;
					case WATER:
						result += " ~";
						break;
					case TESLA_COIL:
						result += " @";
						break;
					default:
						break;
					}

				}

				result += "] ";
			}
			result += "\n";
		}

		return result;

	}

	/**
	 * Resets the selection
	 */
	public void resetSelection() {
		selectedUnit = null;
		actionType = null;

	}

	/**
	 * Removes any unit on the board that has an HP <= 0
	 * 
	 * @return a list of the defeated units
	 */
	public List<Unit> clearDefeatedUnits() {
		List<Unit> result = new LinkedList<>();
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				Tile currentTile = map[row][col];
				if (currentTile.getCurrentUnit() != null) {
					// Determine if the unit should be dead
					Unit currentUnit = currentTile.getCurrentUnit();
					int hp = currentUnit.getStat(StatType.HP);
					if (hp <= 0) {
						removeUnit(currentUnit);
						result.add(currentUnit);
					}
				}
			}
		}
		return result;

	}

	public int getRows() {
		return map.length;
	}

	public List<Tile> getFriendlyUnitsInRange(Unit unit) {
		List<Tile> result = getUnitsInRange(unit);
		List<Tile> unitsToRemove = new ArrayList<>();
		for (Tile u : result) {
			if (u.getCurrentUnit().getTeam() != Team.PLAYER) {
				unitsToRemove.add(u);
			}
		}
		System.out.println(result);
		result.removeAll(unitsToRemove);
		System.out.println(result);
		return result;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public Tile[][] getMapAsArray() {
		return map;
	}

	public int getCols() {
		return map[0].length;
	}

}
