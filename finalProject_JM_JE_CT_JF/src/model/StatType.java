/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public enum StatType implements Serializable {
	ATTACK, HP, DEFENSE, MOVEMENT, ATTACK_RANGE, ACTION_POINTS, SPECIAL_ACTION_USAGE, EXPERIENCE

}
