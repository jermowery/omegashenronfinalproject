/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public enum ActionType implements Serializable {
	SPECIAL, MOVE, ATTACK, NOTHING, SHOP, INVENTORY, TRADE

}
