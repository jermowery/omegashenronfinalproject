/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import model.ActionType;
import model.Coordinate;
import model.Game;
import model.GameState;
import model.Inventory;
import model.MapStrategy;
import model.ScenarioStrategy;
import model.ScenarioType;
import model.Team;
import model.Tile;
import model.TileType;
import model.Unit;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 *         The Frame for the game
 */
@SuppressWarnings("serial")
public class GameGUI extends JFrame implements Observer {
	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class MovementListener implements ActionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if (currentRow == rowToMoveTo && currentCol == colToMoveTo) {
				shouldMove = false;
				unitToAnimate = null;
				isAnimating = false;
				movementTimer.stop();
			} else {
				System.out.println("Current col: " + currentCol);
				System.out.println("Current row: " + currentRow);
				if (currentCol != colToMoveTo) {
					System.out.println("Need to change the col!");
					if (currentCol > colToMoveTo) {
						System.out.println("Make the col smaller!");
						currentCol -= 1;
					} else {
						System.out.println("Make the col larger!");
						currentCol += 1;
					}
				}
				if (currentRow != rowToMoveTo) {
					System.out.println("Need to change the row");
					if (currentRow > rowToMoveTo) {
						System.out.println("Make the row smaller");
						currentRow -= 1;
					} else {
						System.out.println("Make the row larger");
						currentRow += 1;
					}
				}
				System.out.println("Repainting!");
				gamePanel.repaint();

			}

		}

	}

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class AttackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			shouldAttack = !shouldAttack;
			gamePanel.repaint();
			if (game.getState() != GameState.ATTACKING
					&& game.getState() != GameState.SPECIAL) {
				shouldAttack = false;
				unitToAnimate = null;
				attackTimer.stop();
			}
		}

	}

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class CheatKeyListener implements KeyListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e) {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				saveGame.setEnabled(false);
				newGame.setEnabled(false);
				endTurn.setEnabled(false);
				actionsPanel.disableButtons();
				swapPannels(scrollPane, cheatPanel);
				validate();
				repaint();
			}

		}

	}

	private GamePanel gamePanel;
	private StatsPanel statsPanel;
	private ActionsPanel actionsPanel;
	private Game game;
	private JLabel money;
	private JLabel turnCount;
	private JScrollPane scrollPane;
	private List<Coordinate> tilesToApplyActionTo;
	private ShopPanel shopPanel;
	private InventoryPanel inventoryPanel;
	private CheatPanel cheatPanel;
	private JMenuBar playerMenuBar;
	private JMenuItem saveGame;
	private JMenuItem endTurn;
	private JMenuItem newGame;
	private boolean shouldAttack;
	private Timer attackTimer;
	private Unit unitToAnimate;
	private int rowToMoveTo;
	private int colToMoveTo;
	private Timer movementTimer;
	private int currentRow;
	private int currentCol;
	private boolean shouldMove;
	private boolean isAnimating;

	public GameGUI() {

		// Create the game
		game = Game.getInstance();
		game.loadGame();
		tilesToApplyActionTo = null;
		// Add this as an observer
		game.addObserver(this);
		// System.out.println("Scenario: " + game.getScenarioType());
		// Initalize the panels
		gamePanel = new GamePanel();
		statsPanel = new StatsPanel();
		actionsPanel = new ActionsPanel();
		scrollPane = new JScrollPane(gamePanel);
		scrollPane.setVisible(true);
		scrollPane.setPreferredSize(new Dimension(605, 450));
		// Set the layout manager
		setLayout(new BorderLayout());

		// Set the properties of this Frame
		setSize(910, 725);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Hoovervillez Game o' Fun and Discoveriez");

		// Add the frames to the panel
		setUpGameMenu();
		// add(playerInfoPanel, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.WEST);
		add(statsPanel, BorderLayout.EAST);
		add(actionsPanel, BorderLayout.SOUTH);
		shopPanel = new ShopPanel(game.getStoreItems());
		inventoryPanel = new InventoryPanel(new Inventory());
		this.addKeyListener(new CheatKeyListener());
		cheatPanel = new CheatPanel();
	}

	private void setUpGameMenu() {
		playerMenuBar = new JMenuBar();
		JMenu gameOptions = new JMenu("Game Options");
		newGame = new JMenuItem("New Game");
		newGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				int answer = JOptionPane.showConfirmDialog(null,
						"Are you sure you want to start a new game?", null,
						JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.YES_OPTION) {
					GameLauncher launcher = new GameLauncher();
					launcher.setVisible(true);
					setVisible(false);
					dispose();
				}
			}
		});

		saveGame = new JMenuItem("Save Game");
		saveGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File saveGame = new File("resources/savedGame.txt");
				try {
					FileOutputStream outputStream = new FileOutputStream(
							saveGame);
					ObjectOutputStream objectOut = new ObjectOutputStream(
							outputStream);
					objectOut.writeObject(Game.getInstance());
					objectOut.close();
					outputStream.close();

				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					// System.out.println("Error 000x091693: File not found.");
				} catch (IOException e1) {
					e1.printStackTrace();
					// System.out
					// .println("Error 000x120313: Game could not be saved.");
				}

			}
		});

		gameOptions.add(newGame);
		gameOptions.add(saveGame);
		playerMenuBar.add(gameOptions);

		JMenuItem objectives = new JMenuItem("Objectives");
		objectives.setSize(50, 25);
		objectives.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(
						null,
						ScenarioStrategy
								.getScenarioAsString(ScenarioType.KILL_ALL_ENEMIES)
								+ "\n"
								+ ScenarioStrategy.getScenarioAsString(game
										.getScenarioType()));
			}
		});
		playerMenuBar.add(objectives);

		money = new JLabel("Money: $"
				+ Game.getInstance().getCurrentPlayer().getMoney());
		playerMenuBar.add(money);
		turnCount = new JLabel("      Turn Count: "
				+ Game.getInstance().getTurnCount());
		playerMenuBar.add(turnCount);
		endTurn = new JMenuItem("End Turn");
		endTurn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				statsPanel.updateStats(null);
				game.endTurn();
				turnCount.setText("      Turn count: " + game.getTurnCount()
						+ "");
			}
		});
		playerMenuBar.add(endTurn);
		this.add(playerMenuBar, BorderLayout.NORTH);

	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("Updated! Game State: " + game.getState());
		if (game.getState() == GameState.SELECTED_TILE) {
			statsPanel.updateStats(game.getSelectedTile());
			actionsPanel.disableButtons();
			actionsPanel.setSpecialText("Special");
			tilesToApplyActionTo = null;
			gamePanel.repaint();
		} else if (game.getState() == GameState.SELECTING_UNIT) {
			money.setText("Money: $" + game.getCurrentPlayer().getMoney());
			statsPanel.updateStats(game.getSelectedTile());
			actionsPanel.disableButtons();
			tilesToApplyActionTo = null;
			actionsPanel.setSpecialText("Special");
			repaint();
			tilesToApplyActionTo = null;
			gamePanel.repaint();
		} else if (game.getState() == GameState.SELECTED_UNIT) {
			tilesToApplyActionTo = null;
			statsPanel.updateStats(game.getSelectedTile());
			gamePanel.repaint();
			if (game.getSelectedTile().getCurrentUnit().getTeam() == Team.PLAYER) {
				actionsPanel.refreshButtons(game.getSelectedTile()
						.getCurrentUnit().getActions());
				actionsPanel.setSpecialText(game.getSelectedTile()
						.getCurrentUnit().getSpecial());
			} else {
				actionsPanel.disableButtons();
				actionsPanel.setSpecialText("Special");
			}
		} else if (game.getState() == GameState.ATTACKING
				|| game.getState() == GameState.TRADING
				|| game.getState() == GameState.MOVING
				|| game.getState() == GameState.SPECIAL) {
			// We need to get the units to apply the action to from the game
			if (game.getState() == GameState.ATTACKING
					|| game.getState() == GameState.SPECIAL) {
				unitToAnimate = game.getSelectedTile().getCurrentUnit();
				animateAttackUnit();
			}
			List<Tile> tiles = game.getTilesToApplyAction();
			tilesToApplyActionTo = new ArrayList<>();
			for (Tile t : tiles) {
				tilesToApplyActionTo.add(game.getCoordinate(t));
			}
			repaint();
			// conver those into the row and column on the board
			// Repaint board with an indicator on those tiles that they can be
			// attacked

		} else if (game.getState() == GameState.SHOPPING) {
			saveGame.setEnabled(false);
			newGame.setEnabled(false);
			endTurn.setEnabled(false);
			actionsPanel.disableButtons();
			actionsPanel.enableTheShop();
			actionsPanel.setShopText("Close");
			setUpShopPanel();
			swapPannels(scrollPane, shopPanel);
		} else if (game.getState() == GameState.EXITING_SHOP) {
			saveGame.setEnabled(true);
			newGame.setEnabled(true);
			endTurn.setEnabled(true);
			actionsPanel.setShopText("Shop");
			actionsPanel.disableButtons();
			money.setText("$" + game.getCurrentPlayer().getMoney() + "");
			swapPannels(shopPanel, scrollPane);
		} else if (game.getState() == GameState.INVENTORY) {
			saveGame.setEnabled(false);
			newGame.setEnabled(false);
			endTurn.setEnabled(false);
			actionsPanel.disableButtons();
			actionsPanel.enableTheInventory();
			actionsPanel.setInventoryText("Close");
			System.out.println("Number of items is: "
					+ game.getSelectedTile().getCurrentUnit().getInventory()
							.getAllItems().size());
			inventoryPanel.setInventory(game.getSelectedTile().getCurrentUnit()
					.getInventory());
			swapPannels(scrollPane, inventoryPanel);
		} else if (game.getState() == GameState.EXITING_INVENTORY) {
			tilesToApplyActionTo = null;
			saveGame.setEnabled(true);
			newGame.setEnabled(true);
			endTurn.setEnabled(true);
			actionsPanel.setInventoryText("Inventory");
			actionsPanel.disableButtons();
			swapPannels(inventoryPanel, scrollPane);
			validate();
			repaint();
		} else if (game.getState() == GameState.LOST) {
			repaint();
			JOptionPane.showMessageDialog(null, "HAHA you lost!");
			System.exit(0);
		} else if (game.getState() == GameState.WON) {
			repaint();
			JOptionPane.showMessageDialog(null, "You won!");
			System.exit(0);
		} else if (game.getState() == GameState.SELECTING_ITEMS) {
			saveGame.setEnabled(false);
			newGame.setEnabled(false);
			endTurn.setEnabled(false);
			actionsPanel.disableButtons();
			actionsPanel.enableTheInventory();
			actionsPanel.setInventoryText("Close");
			inventoryPanel.setUnitTradingTo((Unit) arg);
			inventoryPanel.setInventory(game.getSelectedTile().getCurrentUnit()
					.getInventory());

			swapPannels(scrollPane, inventoryPanel);
		} else if (game.getState() == GameState.CHEATED) {
			tilesToApplyActionTo = null;
			saveGame.setEnabled(true);
			newGame.setEnabled(true);
			endTurn.setEnabled(true);
			actionsPanel.disableButtons();
			swapPannels(cheatPanel, scrollPane);
			validate();
			repaint();
		}

	}

	private void animateAttackUnit() {
		shouldAttack = true;
		attackTimer = new Timer(1000, new AttackListener());
		attackTimer.start();
	}

	private void swapPannels(Component currentPanel, Component panelToSwap) {
		Component[] components = getComponents();
		int location = -1;
		for (int index = 0; index < components.length; index++) {
			if (components[index].equals(currentPanel)) {
				location = index;
			}
		}
		GameGUI.this.remove(currentPanel);
		GameGUI.this.add(panelToSwap, location);
		revalidate();
		repaint();
	}

	private void setUpShopPanel() {
		shopPanel.setList(game.getStoreItems());
		shopPanel.setPreferredSize(new Dimension(605, 450));

	}

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 * 
	 *         This JPanel will hold the game itself, with the graphics and
	 *         handle the animations This is where the game board and units are
	 *         shown
	 */
	private class GamePanel extends JPanel implements MouseListener {
		/**
		 * The instance of the game being played.
		 */
		private Game game;

		/**
		 * Constructor which adds all the necesary elements for this JPanel
		 */
		@SuppressWarnings("incomplete-switch")
		public GamePanel() {
			super();
			this.addMouseListener(this);
			game = Game.getInstance();
			isAnimating = false;
			// Load all images
			ImageFactory.getInstance();
			int height = 0;
			int width = 0;
			switch (game.getMapType()) {
			case EASY:
				height = ViewStrategy.TILE_HEIGHT * MapStrategy.EASY_MAP_SIZE;
				width = ViewStrategy.TILE_WIDTH * MapStrategy.EASY_MAP_SIZE;
				break;
			case HARD:
				height = ViewStrategy.TILE_HEIGHT * MapStrategy.HARD_MAP_SIZE;
				width = ViewStrategy.TILE_WIDTH * MapStrategy.HARD_MAP_SIZE;
				break;
			case MEDIUM:
				height = ViewStrategy.TILE_HEIGHT * MapStrategy.MEDIUM_MAP_SIZE;
				width = ViewStrategy.TILE_WIDTH * MapStrategy.MEDIUM_MAP_SIZE;
				break;
			case RANDOM:
				height = ViewStrategy.TILE_HEIGHT * MapStrategy.RANDOM_MAP_SIZE;
				width = ViewStrategy.TILE_WIDTH * MapStrategy.RANDOM_MAP_SIZE;
				break;
			}
			setPreferredSize(new Dimension(width, height));
			JPanel dummyJPanel = new JPanel();
			dummyJPanel.setSize(900, 900);
			this.add(dummyJPanel);
			System.out.println(width);
			System.out.println(height);

		}

		// *******************************************//
		// ------------Warning paintComponent--------//
		// ----------is half-baked only a start-----//
		// ****************************************//

		@Override
		protected void paintComponent(Graphics g) {
			// TODO animate the panel
			super.paintComponent(g);
			// System.out.println("Paint component being called!");
			Graphics2D g2 = (Graphics2D) g;

			Tile[][] gameBoard = game.getGameAsArray();

			BufferedImage imageToPaint = null;
			for (int row = 0; row < gameBoard.length; row++) {
				for (int column = 0; column < gameBoard[0].length; column++) {
					if (gameBoard[row][column].getTileType() == TileType.DESERT) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.DESERT);
					} else if (gameBoard[row][column].getTileType() == TileType.WATER) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.WATER);
					} else if (gameBoard[row][column].getTileType() == TileType.WALL) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.WALL);
					} else if (gameBoard[row][column].getTileType() == TileType.RUBBLE) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.RUBBLE);
					} else if (gameBoard[row][column].getTileType() == TileType.ITEM) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.ITEM);
					} else if (gameBoard[row][column].getTileType() == TileType.TESLA_COIL) {
						imageToPaint = ImageFactory.getInstance()
								.getTerrainImage(TileType.TESLA_COIL);
					}
					g2.drawImage(imageToPaint,
							column * ViewStrategy.TILE_WIDTH, row
									* ViewStrategy.TILE_HEIGHT,
							ViewStrategy.TILE_WIDTH, ViewStrategy.TILE_HEIGHT,
							null);
					// System.out.println("Just drew the terrain!");
					if (tilesToApplyActionTo != null) {
						// System.out
						// .println("Figured out the tiles were not null!");
						for (int index = 0; index < tilesToApplyActionTo.size(); index++) {
							int tileRow = tilesToApplyActionTo.get(index)
									.getRow();
							int tileCol = tilesToApplyActionTo.get(index)
									.getColumn();
							if (tileRow == row && tileCol == column) {
								// System.out.println("Tried to draw at "
								// + tileRow + "," + tileCol);
								g2.drawImage(ImageFactory.getInstance()
										.getOverlay(game.getState()), column
										* ViewStrategy.TILE_WIDTH, row
										* ViewStrategy.TILE_HEIGHT,
										ViewStrategy.TILE_WIDTH,
										ViewStrategy.TILE_HEIGHT, null);
							}
						}
					}
					if (gameBoard[row][column].getCurrentUnit() != null) {
						if (unitToAnimate != null
								&& gameBoard[row][column].getCurrentUnit() == unitToAnimate) {
							if (shouldAttack) {
								imageToPaint = ImageFactory.getInstance()
										.getUnitAttackImage(
												gameBoard[row][column]
														.getCurrentUnit()
														.getType());
							} else {
								imageToPaint = ImageFactory.getInstance()
										.getUnitDefaultImage(
												gameBoard[row][column]
														.getCurrentUnit()
														.getType());
							}
						} else {
							imageToPaint = ImageFactory
									.getInstance()
									.getUnitDefaultImage(
											gameBoard[row][column]
													.getCurrentUnit().getType());

						}
						// if(animateUnits){
						// g2.drawImage(imageToPaint, column
						// * ViewStrategy.TILE_WIDTH + 10, row
						// * ViewStrategy.TILE_HEIGHT + 10,
						// ViewStrategy.TILE_WIDTH,
						// ViewStrategy.TILE_HEIGHT, null);
						//
						// animateUnits = false;
						// repaint();
						//
						// }
						// else
						if (unitToAnimate != null
								&& gameBoard[row][column].getCurrentUnit() == unitToAnimate
								&& shouldMove) {
							;
						} else {
							g2.drawImage(imageToPaint, column
									* ViewStrategy.TILE_WIDTH, row
									* ViewStrategy.TILE_HEIGHT,
									ViewStrategy.TILE_WIDTH,
									ViewStrategy.TILE_HEIGHT, null);
						}
						if (gameBoard[row][column].getCurrentUnit().getTeam() == Team.AI) {
							g2.drawImage(ImageFactory.getInstance()
									.getAIOverlay(), column
									* ViewStrategy.TILE_WIDTH, row
									* ViewStrategy.TILE_HEIGHT,
									ViewStrategy.TILE_WIDTH,
									ViewStrategy.TILE_HEIGHT, null);
						}
					}

				}
			}

			if (unitToAnimate != null && shouldMove) {
				g2.drawImage(
						ImageFactory.getInstance().getUnitDefaultImage(
								unitToAnimate.getType()), currentCol
								* ViewStrategy.TILE_WIDTH, currentRow
								* ViewStrategy.TILE_HEIGHT,
						ViewStrategy.TILE_WIDTH, ViewStrategy.TILE_HEIGHT, null);
			}

		}

		@Override
		public void mouseClicked(MouseEvent e) {
			System.out.println("Clicked");
			// In this method we need to convert the coordinates of the click to
			// a
			// row and a column in the map.
			// We can do this by taking the integer part of the division of the
			// x
			// and the width of a tile and the division of a y and the height of
			// a
			// tile
			// Convert the location to a row and column
			int row = e.getY() / ViewStrategy.TILE_HEIGHT;
			System.out.println(row);
			int col = e.getX() / ViewStrategy.TILE_WIDTH;
			System.out.println(col);
			// Now that we have the x and y we need to check with the state

			// If the state is SELECTING_UNITS then we can select the tile here
			// If the state is ATTACKING, MOVING, SPECIAL, TRADE then we will
			// need
			// to get the tile and apply the action to that tile

			// All other cases this panel will not know about, the other panels
			// will
			// need to deal with that
			if (!isAnimating) {
				Game game = Game.getInstance();
				if (game.getState() == GameState.SELECTING_UNIT
						|| game.getState() == GameState.SELECTED_TILE
						|| game.getState() == GameState.SELECTED_UNIT
						|| game.getState() == GameState.EXITING_SHOP
						|| game.getState() == GameState.EXITING_INVENTORY
						|| game.getState() == GameState.CHEATED) {
					game.selectUnit(row, col);
				} else {
					boolean validSelection = false;
					for (Coordinate c : tilesToApplyActionTo) {
						if (c.getRow() == row && c.getColumn() == col) {
							if (game.getSelectedUnitAction() == ActionType.MOVE) {
								isAnimating = true;
								shouldMove = true;
								unitToAnimate = game.getSelectedTile()
										.getCurrentUnit();
								currentRow = game.getCoordinate(
										game.getSelectedTile()).getRow();
								currentCol = game.getCoordinate(
										game.getSelectedTile()).getColumn();
								rowToMoveTo = row;
								colToMoveTo = col;
								animateMoveUnit();
							}
							game.applyAction(row, col);
							validSelection = true;
							repaint();
							break;
						}
					}
					if (!validSelection) {
						game.selectUnit(row, col);
					}

				}
				// in the case we are animating.
			} else {

			}
		}

		private void animateMoveUnit() {
			movementTimer = new Timer(100, new MovementListener());
			movementTimer.start();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

}