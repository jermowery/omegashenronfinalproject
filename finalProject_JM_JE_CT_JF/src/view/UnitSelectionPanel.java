/**
 * 
 */
package view;

import java.awt.LayoutManager;

import javax.swing.JPanel;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This panel is the panel shown when trying to select units at the
 *         start of the game
 */
@SuppressWarnings("serial")
public class UnitSelectionPanel extends JPanel {

	/**
	 * Use this constructor to add the elements this panel needs initially
	 */
	public UnitSelectionPanel() {
		super();
	}

	/**
	 * @param layout
	 */
	public UnitSelectionPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public UnitSelectionPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public UnitSelectionPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

}
