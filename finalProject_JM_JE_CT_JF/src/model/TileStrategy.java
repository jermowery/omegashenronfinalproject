/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu Defines static methods for stat modifications relating to
 *         tiles
 */
@SuppressWarnings("serial")
public class TileStrategy implements Serializable {

	private static final int TESLA_COIL_ATTACK = 0;
	private static final int TESLA_COIL_HP = -5;
	private static final int TESLA_COIL_DEFENSE = 0;
	private static final int TESLA_COIL_MOVEMENT = 0;
	private static final int TESLA_COIL_ATTACK_RANGE = 0;
	private static final int TESLA_COIL_ACTION_POINTS = 0;

	private static final int WATER_ATTACK = 2;
	private static final int WATER_HP = 0;
	private static final int WATER_DEFENSE = -2;
	private static final int WATER_MOVEMENT = -1;
	private static final int WATER_ATTACK_RANGE = 0;
	private static final int WATER_ACTION_POINTS = 0;

	private static final int DESERT_ATTACK = 0;
	private static final int DESERT_HP = 0;
	private static final int DESERT_DEFENSE = 0;
	private static final int DESERT_MOVEMENT = 0;
	private static final int DESERT_ATTACK_RANGE = 0;
	private static final int DESERT_ACTION_POINTS = 0;

	private static final int WALL_ATTACK = 9001;
	private static final int WALL_HP = 9001;
	private static final int WALL_DEFENSE = 9001;
	private static final int WALL_MOVEMENT = 9001;
	private static final int WALL_ATTACK_RANGE = 9001;
	private static final int WALL_ACTION_POINTS = 9001;

	private static final int RUBBLE_ATTACK = -1;
	private static final int RUBBLE_HP = 0;
	private static final int RUBBLE_DEFENSE = 0;
	private static final int RUBBLE_MOVEMENT = -2;
	private static final int RUBBLE_ATTACK_RANGE = 0;
	private static final int RUBBLE_ACTION_POINTS = 0;

	/**
	 * When implemented this method will return a list of StatNode for the type
	 * passed in
	 * 
	 * @param type
	 *            the type of the tile to get the StatNode list for
	 * @return a list of StatNode for the type
	 */
	public static List<StatNode> getStatModifications(TileType type) {
		switch (type) {
		case RUBBLE:
			return getRubbleModifcations();
		case WALL:
			return getWallModifications();
		case DESERT:
			return getDesertModifications();
		case WATER:
			return getWaterModifications();
		case TESLA_COIL:
			return getTeslaModifications();

		default:
			break;
		}
		return null;
	}

	private static List<StatNode> getTeslaModifications() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, TileStrategy.TESLA_COIL_ATTACK));
		result.add(new StatNode(StatType.HP, TileStrategy.TESLA_COIL_HP));
		result.add(new StatNode(StatType.DEFENSE,
				TileStrategy.TESLA_COIL_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT,
				TileStrategy.TESLA_COIL_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				TileStrategy.TESLA_COIL_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				TileStrategy.TESLA_COIL_ACTION_POINTS));
		return result;
	}

	private static List<StatNode> getWaterModifications() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, TileStrategy.WATER_ATTACK));
		result.add(new StatNode(StatType.HP, TileStrategy.WATER_HP));
		result.add(new StatNode(StatType.DEFENSE, TileStrategy.WATER_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, TileStrategy.WATER_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				TileStrategy.WATER_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				TileStrategy.WATER_ACTION_POINTS));
		return result;
	}

	private static List<StatNode> getDesertModifications() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, TileStrategy.DESERT_ATTACK));
		result.add(new StatNode(StatType.HP, TileStrategy.DESERT_HP));
		result.add(new StatNode(StatType.DEFENSE, TileStrategy.DESERT_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, TileStrategy.DESERT_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				TileStrategy.DESERT_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				TileStrategy.DESERT_ACTION_POINTS));
		return result;
	}

	private static List<StatNode> getWallModifications() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, TileStrategy.WALL_ATTACK));
		result.add(new StatNode(StatType.HP, TileStrategy.WALL_HP));
		result.add(new StatNode(StatType.DEFENSE, TileStrategy.WALL_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, TileStrategy.WALL_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				TileStrategy.WALL_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				TileStrategy.WALL_ACTION_POINTS));
		return result;
	}

	private static List<StatNode> getRubbleModifcations() {
		List<StatNode> result = new LinkedList<>();
		result.add(new StatNode(StatType.ATTACK, TileStrategy.RUBBLE_ATTACK));
		result.add(new StatNode(StatType.HP, TileStrategy.RUBBLE_HP));
		result.add(new StatNode(StatType.DEFENSE, TileStrategy.RUBBLE_DEFENSE));
		result.add(new StatNode(StatType.MOVEMENT, TileStrategy.RUBBLE_MOVEMENT));
		result.add(new StatNode(StatType.ATTACK_RANGE,
				TileStrategy.RUBBLE_ATTACK_RANGE));
		result.add(new StatNode(StatType.ACTION_POINTS,
				TileStrategy.RUBBLE_ACTION_POINTS));
		return result;
	}

	/**
	 * When implemented this method will return a list of ActionNode for the
	 * type passed in
	 * 
	 * @param type
	 *            the type of the tile to get the ActionNode list for
	 * @return a list of ActionNode for the type
	 */
	public static List<ActionNode> getTileActions(TileType type) {
		return null;
	}

	public static String getName(TileType tileType) {
		switch (tileType) {
		case DESERT:
			return "Desert";
		case ITEM:
			return "Item";
		case RUBBLE:
			return "Rubble";
		case TESLA_COIL:
			return "Tesla Coil";
		case WALL:
			return "Wall";
		case WATER:
			return "Water";
		default:
			return "Impossible";

		}
	}
}
