/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class TerrainStatPanel extends AbstractStatPanel {

	/**
	 * 
	 */
	public TerrainStatPanel() {
		this.setPreferredSize(new Dimension(305, 100));
		setLayout(new GridLayout(ViewStrategy.TERRAIN_NUM_ROWS,
				ViewStrategy.TERRAIN_NUM_COLS));

	}

	/**
	 * @param layout
	 */
	public TerrainStatPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public TerrainStatPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public TerrainStatPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

}
