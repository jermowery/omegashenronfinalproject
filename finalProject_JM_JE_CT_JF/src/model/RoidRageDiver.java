package model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class RoidRageDiver extends Unit implements Serializable {

	public RoidRageDiver() {
		super();
		setType(UnitType.ROIDRAGEDIVER);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.ROIDRAGEDIVER));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.ROIDRAGEDIVER));
		super.setStatList(stats);
	}
}
