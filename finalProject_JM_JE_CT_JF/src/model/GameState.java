/**
 * 
 */
package model;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         Enum for the current state of the game
 */
public enum GameState {

	ATTACKING, MOVING, TRADING, SPECIAL, INVENTORY, SHOPPING, SELECTING_UNIT, NEW_GAME, SELECTED_UNIT, SELECTED_TILE, EXITING_SHOP, EXITING_INVENTORY, LOST, WON, SELECTING_ITEMS, CHEATED;

}
