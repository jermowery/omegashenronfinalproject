/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class StatStrategy implements Serializable {

	// Weaver
	public static final int WEAVER_ATTACK = 5;
	public static final int WEAVER_HP = 15;
	public static final int WEAVER_DEFENSE = 10;
	public static final int WEAVER_MOVEMENT = 4;
	public static final int WEAVER_ATTACK_RANGE = 7;
	public static final int WEAVER_ACTION_POINTS = 3;
	public static final int WEAVER_SPECIAL_ACTION_USAGE = 1;

	// Tesla
	public static final int TESLA_ATTACK = 15;
	public static final int TESLA_HP = 15;
	public static final int TESLA_DEFENSE = 7;
	public static final int TESLA_MOVEMENT = 4;
	public static final int TESLA_ATTACK_RANGE = 7;
	public static final int TESLA_ACTION_POINTS = 2;
	public static final int TESLA_SPECIAL_ACTION_USAGE = 2;

	// Trooper
	public static final int TROOPER_ATTACK = 15;
	public static final int TROOPER_HP = 30;
	public static final int TROOPER_DEFENSE = 15;
	public static final int TROOPER_MOVEMENT = 5;
	public static final int TROOPER_ATTACK_RANGE = 1;
	public static final int TROOPER_ACTION_POINTS = 2;
	public static final int TROOPER_SPECIAL_ACTION_USAGE = 2;

	// Roid Rage Diver
	public static final int ROID_RAGE_DIVER_ATTACK = 50;
	public static final int ROID_RAGE_DIVER_HP = 40;
	public static final int ROID_RAGE_DIVER_DEFENSE = 50;
	public static final int ROID_RAGE_DIVER_MOVEMENT = 3;
	public static final int ROID_RAGE_DIVER_ATTACK_RANGE = 1;
	public static final int ROID_RAGE_DIVER_ACTION_POINTS = 1;
	public static final int ROID_RAGE_DIVER_SPECIAL_ACTION_USAGE = 1;

	// Gearhead
	public static final int GEARHEAD_ATTACK = 20;
	public static final int GEARHEAD_HP = 20;
	public static final int GEARHEAD_DEFENSE = 10;
	public static final int GEARHEAD_MOVEMENT = 7;
	public static final int GEARHEAD_ATTACK_RANGE = 1;
	public static final int GEARHEAD_ACTION_POINTS = 2;
	public static final int GEARHEAD_SPECIAL_ACTION_USAGE = 2;

	// Dylan
	public static final int DYLAN_ACTION_POINTS = 1001;
	public static final int DYLAN_ATTACK = 9001;
	public static final int DYLAN_HP = 1000;
	public static final int DYLAN_DEFENSE = 1000;
	public static final int DYLAN_MOVEMENT = 100;
	public static final int DYLAN_ATTACK_RANGE = 1000;
	public static final int DYLAN_SPECIAL_ACTION_USAGE = 1;

	// DR Mercer
	public static final int DRMERCER_ATTACK = 9001;
	public static final int DRMERCER_HP = 1000;
	public static final int DRMERCER_DEFENSE = 1000;
	public static final int DRMERCER_MOVEMENT = 100;
	public static final int DRMERCER_ATTACK_RANGE = 1000;
	public static final int DRMERCER_ACTION_POINTS = 1001;
	public static final int DRMERCER_SPECIAL_ACTION_USAGE = 1;

	// Annoying Brat
	public static final int ANNOYING_BRAT_ATTACK = 5;
	public static final int ANNOYING_BRAT_HP = 10;
	public static final int ANNOYING_BRAT_DEFENSE = 5;
	public static final int ANNOYING_BRAT_MOVEMENT = 7;
	public static final int ANNOYING_BRAT_ATTACK_RANGE = 1;
	public static final int ANNOYING_BRAT_ACTION_POINTS = 2;
	public static final int ANNOYING_BRAT_SPECIAL_ACTION_USAGE = 2;

	// Eric
	public static final int ERIC_ATTACK = 9001;
	public static final int ERIC_HP = 1000;
	public static final int ERIC_DEFENSE = 1000;
	public static final int ERIC_MOVEMENT = 100;
	public static final int ERIC_ATTACK_RANGE = 1000;
	public static final int ERIC_ACTION_POINTS = 1001;
	public static final int ERIC_SPECIAL_ACTION_USAGE = 1;

	public static final int DEFAULT_EXPERIENCE = 0;

	// Commander
	public static final int COMMANDER_ATTACK = 25;
	public static final int COMMANDER_HP = 75;
	public static final int COMMANDER_DEFENSE = 60;
	public static final int COMMANDER_MOVEMENT = 4;
	public static final int COMMANDER_ATTACK_RANGE = 3;
	public static final int COMMANDER_ACTION_POINTS = 1;
	public static final int COMMANDER_SPECIAL_ACTION_USAGE = 1;

	public static String getStatName(StatType statType) {
		switch (statType) {
		case ACTION_POINTS:
			return "Action Points";
		case ATTACK:
			return "Attack";
		case ATTACK_RANGE:
			return "Attack Range";
		case DEFENSE:
			return "Defense";
		case HP:
			return "HP";
		case MOVEMENT:
			return "Movement";
		case SPECIAL_ACTION_USAGE:
			return "Special Action Usage";
		case EXPERIENCE:
			return "Experience";
		default:
			return null;
		}
	}

}
