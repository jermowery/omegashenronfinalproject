package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 */

@SuppressWarnings("serial")
public class ItemStrategy implements Serializable {

	private static List<Item> allItems;

	/**
	 * Calls createItems first then returns the list
	 * 
	 * @return list of items
	 */

	public static List<Item> allItems() {
		allItems = new ArrayList<Item>();
		createItems();

		return allItems;
	}

	/**
	 * Creates all the items and then adds them to the allItems list
	 */

	private static void createItems() {
		// allItems.add(new Item(ItemType.HEAL, 5));
		// allItems.add(new Item(ItemType.ATTACK, 8));
		// allItems.add(new Item(ItemType.DEFENSE, 8));

		allItems.add(new Item(ItemType.GRADE_S_HEALTH_TONIC, 5));
		allItems.add(new Item(ItemType.GRADE_B_HEALTH_TONIC, 10));
		allItems.add(new Item(ItemType.GRADE_A_HEALTH_TONIC, 15));
		allItems.add(new Item(ItemType.SIRGEOS_STRENGTH_SHAMPOO, 12));
		allItems.add(new Item(ItemType.RAD_BIKER_GLASSES, 9));
		allItems.add(new Item(ItemType.SKATE_SHOES, 6));
		allItems.add(new Item(ItemType.POLO_CLUB_ROGUE, 18));
		allItems.add(new Item(ItemType.SHARK_CLOAK, 10));
		allItems.add(new Item(ItemType.WIZARDS_DIARY, 20));
		allItems.add(new Item(ItemType.SCARY_SPIDERS, 6));
		allItems.add(new Item(ItemType.FOUNTAINHEAD, 12));
		allItems.add(new Item(ItemType.RAGE_BREW, 11));
		allItems.add(new Item(ItemType.MORAL_BOOST, 10));
		allItems.add(new Item(ItemType.HARPOON, 16));
		allItems.add(new Item(ItemType.TALKING_SWORD, 10));
		allItems.add(new Item(ItemType.SHAVING_RAZOR, 18));
		allItems.add(new Item(ItemType.LEAKY_PEN, 25));
		allItems.add(new Item(ItemType.CANNON, 20));
		allItems.add(new Item(ItemType.BIGGER_CANNON, 30));
		allItems.add(new Item(ItemType.BIGGER_CANNON, 50));
		allItems.add(new Item(ItemType.KOOL_AID, 10));

	}

	/**
	 * Takes in an ItemType and returns the name of the item
	 * 
	 * @param ItemType
	 * @return the name as a String
	 */

	public static String getName(ItemType type) {
		switch (type) {
		case GRADE_S_HEALTH_TONIC:
			return "Grade S Health Tonic";
		case GRADE_B_HEALTH_TONIC:
			return "Grade B Health Tonic";
		case GRADE_A_HEALTH_TONIC:
			return "Grade A Health Tonic";
		case SIRGEOS_STRENGTH_SHAMPOO:
			return "Sirgeo's Strength Shampoo";
		case RAD_BIKER_GLASSES:
			return "Rad Biker Glasses";
		case SKATE_SHOES:
			return "Skate shoes";
		case POLO_CLUB_ROGUE:
			return "Polo Club Rogue";
		case SHARK_CLOAK:
			return "Shark Cloak";
		case WIZARDS_DIARY:
			return "Wizard's Diary";
		case SCARY_SPIDERS:
			return "Scary Spiders";
		case FOUNTAINHEAD:
			return "Fountainhead";
		case RAGE_BREW:
			return "Rage Brew";
		case MORAL_BOOST:
			return "Moral Boost";
		case HARPOON:
			return "Harpoon";
		case TALKING_SWORD:
			return "Talking Sword";
		case SHAVING_RAZOR:
			return "Shaving Razor";
		case LEAKY_PEN:
			return "Leaky Pen";
		case CANNON:
			return "Cannon";
		case BIGGER_CANNON:
			return "Bigger Cannon";
		case THE_OVERCOMPENSATOR:
			return "THE OVERCOMPENSATOR";
		case KOOL_AID:
			return "Kool-Aid";

			// old test case items, left in the game.
		case HEAL:
			return "Heal";
		case ATTACK:
			return "Attack";
		case DEFENSE:
			return "Defense";
		default:
			return "";
		}
	}

	/**
	 * Returns a String of the item description for a given ItemType
	 * 
	 * @param ItemType
	 * @return String
	 */

	public static String getDescription(ItemType type) {
		switch (type) {
		case GRADE_S_HEALTH_TONIC:
			return "A terribly small health boost.";
		case GRADE_B_HEALTH_TONIC:
			return "An okay health boost, I guess.";
		case GRADE_A_HEALTH_TONIC:
			return "Now we're talking!";
		case SIRGEOS_STRENGTH_SHAMPOO:
			return "Shampoo for real men who have beards.";
		case RAD_BIKER_GLASSES:
			return "Suprisingly helps your eyesight.";
		case SKATE_SHOES:
			return "Every kid has gotta have them.";
		case POLO_CLUB_ROGUE:
			return "A really nice and sophisticated cologne.";
		case SHARK_CLOAK:
			return "Don't ask how they made it.";
		case WIZARDS_DIARY:
			return "Every few pages are boyband song lyrics.";
		case SCARY_SPIDERS:
			return "Really creeps some folks out.";
		case FOUNTAINHEAD:
			return "A really lame book.";
		case RAGE_BREW:
			return "Recently legalized despite public outcry.";
		case MORAL_BOOST:
			return "Bares a stricking resembalance to a Hallmark card.";
		case HARPOON:
			return "Careful, this thing is pointy.";
		case TALKING_SWORD:
			return "Won't shut up about some guy named Chad.";
		case SHAVING_RAZOR:
			return "Zip-Zop, enemies are ripped to shreds.";
		case LEAKY_PEN:
			return "This pen runs circles around swords.";
		case CANNON:
			return "A bit mighter than the pen.";
		case BIGGER_CANNON:
			return "Some say this cannon is \"Serious Business.\"";
		case THE_OVERCOMPENSATOR:
			return "Gurl how big a cannon you need?!";
		case KOOL_AID:
			return "Pretty sure it isn't posioned . . .";
		case HEAL:
			return "Heal";
		case ATTACK:
			return "Attack";
		case DEFENSE:
			return "Defense";
		default:
			return "";
		}
	}

	/**
	 * Returns a List of StatNodes for a given ItemType
	 * 
	 * @param ItemType
	 * @return a List<StatNode>
	 */

	public static List<StatNode> getStats(ItemType type) {

		List<StatNode> tempList = new ArrayList<StatNode>();

		switch (type) {
		case GRADE_S_HEALTH_TONIC:
			tempList.add(new StatNode(StatType.HP, 2));
			break;
		case GRADE_B_HEALTH_TONIC:
			tempList.add(new StatNode(StatType.HP, 6));
			break;
		case GRADE_A_HEALTH_TONIC:
			tempList.add(new StatNode(StatType.HP, 12));
			break;
		case SIRGEOS_STRENGTH_SHAMPOO:
			tempList.add(new StatNode(StatType.ATTACK, 3));
			break;
		case RAD_BIKER_GLASSES:
			tempList.add(new StatNode(StatType.ATTACK_RANGE, 2));
			break;
		case SKATE_SHOES:
			tempList.add(new StatNode(StatType.MOVEMENT, 2));
			break;
		case POLO_CLUB_ROGUE:
			tempList.add(new StatNode(StatType.ATTACK, 3));
			tempList.add(new StatNode(StatType.DEFENSE, 3));
			break;
		case SHARK_CLOAK:
			tempList.add(new StatNode(StatType.DEFENSE, 3));
			break;
		case WIZARDS_DIARY:
			tempList.add(new StatNode(StatType.HP, 2));
			tempList.add(new StatNode(StatType.ATTACK, 3));
			tempList.add(new StatNode(StatType.DEFENSE, 3));
			tempList.add(new StatNode(StatType.HP, 2));
			break;
		case SCARY_SPIDERS:
			tempList.add(new StatNode(StatType.DEFENSE, 1));
			break;
		case FOUNTAINHEAD:
			tempList.add(new StatNode(StatType.DEFENSE, 4));
			tempList.add(new StatNode(StatType.ATTACK, -2));
			break;
		case RAGE_BREW:
			tempList.add(new StatNode(StatType.ATTACK, 6));
			tempList.add(new StatNode(StatType.DEFENSE, -4));
			break;
		case MORAL_BOOST:
			tempList.add(new StatNode(StatType.HP, 1));
			tempList.add(new StatNode(StatType.ATTACK, 1));
			tempList.add(new StatNode(StatType.DEFENSE, 1));
			tempList.add(new StatNode(StatType.MOVEMENT, 1));
			break;
		case HARPOON:
			tempList.add(new StatNode(StatType.ATTACK, 2));
			tempList.add(new StatNode(StatType.ATTACK_RANGE, 2));
			tempList.add(new StatNode(StatType.DEFENSE, -2));
			break;
		case TALKING_SWORD:
			tempList.add(new StatNode(StatType.ATTACK, 4));
			tempList.add(new StatNode(StatType.DEFENSE, -4));
			break;
		case SHAVING_RAZOR:
			tempList.add(new StatNode(StatType.ATTACK, 6));
			break;
		case LEAKY_PEN:
			tempList.add(new StatNode(StatType.ATTACK, 10));
			break;
		case CANNON:
			tempList.add(new StatNode(StatType.ATTACK, 10));
			tempList.add(new StatNode(StatType.DEFENSE, -2));
			break;
		case BIGGER_CANNON:
			tempList.add(new StatNode(StatType.ATTACK, 15));
			tempList.add(new StatNode(StatType.DEFENSE, -2));
			break;
		case THE_OVERCOMPENSATOR:
			tempList.add(new StatNode(StatType.ATTACK, 20));
			tempList.add(new StatNode(StatType.DEFENSE, -2));
			break;
		case KOOL_AID:
			tempList.add(new StatNode(StatType.HP, 2));
			tempList.add(new StatNode(StatType.DEFENSE, 2));
			break;
		case ACTION_POINT:
			break;
		case ATTACK:
			break;
		case ATTACK_RANGE:
			break;
		case DEFENSE:
			break;
		case DRAGONBALLS:
			break;
		case GOD_MODE:
			break;
		case HEAL:
			break;
		case HEALTH:
			break;
		case MOVE:
			break;
		default:
			break;
		}

		return tempList;
	}

}