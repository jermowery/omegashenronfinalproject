package test;

import java.util.ArrayList;
import java.util.List;

import model.Gearhead;
import model.Map;
import model.MapType;
import model.Team;
import model.Unit;

import org.junit.Test;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */

public class TestMap {

	@Test
	public void testGenerateEasyMap() {
		List<Unit> player = new ArrayList<>();
		List<Unit> ai = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			player.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			ai.add(new Gearhead());
		}

	}

	@Test
	public void testGenerateMediumMap() {
		List<Unit> player = new ArrayList<>();
		List<Unit> ai = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			player.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			ai.add(new Gearhead());
		}
	}

	@Test
	public void testGenerateHardMap() {
		List<Unit> player = new ArrayList<>();
		List<Unit> ai = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			player.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			ai.add(new Gearhead());
		}
	}

	@Test
	public void testRandomMap() {
		List<Unit> player = new ArrayList<>();
		List<Unit> ai = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			player.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			ai.add(new Gearhead());
		}
	}

	@Test
	public void testSelectUnitEasyMap() {
		List<Unit> player = new ArrayList<>();
		List<Unit> ai = new ArrayList<>();

		for (int i = 0; i < 6; i++) {
			player.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			player.get(i).setTeam(Team.PLAYER);
		}

		for (int i = 0; i < 6; i++) {
			ai.add(new Gearhead());
		}

		for (int i = 0; i < 6; i++) {
			ai.get(i).setTeam(Team.AI);
		}

		Map aMap = new Map(MapType.EASY, player, ai);
		// System.out.println(aMap.toString());

		aMap.getPossibleMovementTiles(player.get(0));
		// System.out.println(aMap.toString());

	}

	@Test
	public void testMoveUnitEasyMap() {

		Map aMap = new Map();

		Gearhead dude = new Gearhead();
		dude.setTeam(Team.PLAYER);
		aMap.placeUnit(dude, 5, 5);
		aMap.getPossibleMovementTiles(dude);
		// System.out.println(aMap.toString());
		aMap.moveUnit(dude, 6, 6);
		// System.out.println(aMap.toString());

	}

	@Test
	public void testAttackUnitEasyMap() {

		Map aMap = new Map();

		Gearhead dude = new Gearhead();
		Gearhead badDude = new Gearhead();
		dude.setTeam(Team.PLAYER);
		badDude.setTeam(Team.AI);
		aMap.placeUnit(dude, 5, 5);
		aMap.placeUnit(badDude, 5, 6);
		System.out.println(aMap.getUnitsInRange(dude));
		aMap.moveUnit(dude, 6, 6);
		// System.out.println(aMap.toString());

	}

	@Test
	public void testMapToString() {
		Map testMap = new Map();
		testMap.toString();
		System.out.println(testMap.toString());
	}
}
