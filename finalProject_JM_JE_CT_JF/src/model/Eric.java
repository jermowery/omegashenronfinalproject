/**
 * 
 */
package model;

import java.util.List;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class Eric extends Unit {
	public Eric() {
		super();
		setType(UnitType.ERIC);
		List<ActionNode> result = super.getActions();
		// Adds to the default actions the actions for this specific unit
		result.addAll(UnitStrategy.getUnitActions(UnitType.ERIC));
		super.setActions(result);
		List<StatNode> stats = super.getStatList();
		stats.addAll(UnitStrategy.getUnitDefaultStats(UnitType.ERIC));
		super.setStatList(stats);
	}
}
