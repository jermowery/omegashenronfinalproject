/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */
@SuppressWarnings("serial")
public class Item implements Serializable {
	private List<StatNode> statModifications;
	private ItemType type;
	private String name;
	private int price;

	/**
	 * Constructor takes in an ItemType and the price of the item and then
	 * creates the statModifications list for that item
	 * 
	 * @param otherType
	 *            - ItemType
	 * @param otherPrice
	 *            - int
	 */

	public Item(ItemType otherType, int otherPrice) {
		type = otherType;
		name = ItemStrategy.getName(type);
		statModifications = ItemStrategy.getStats(type);
		price = otherPrice;
	}

	/**
	 * 
	 * Returns the price of this item
	 * 
	 * @return the price
	 */

	public int getPrice() {
		return price;
	}

	/**
	 * Returns the name of this item
	 * 
	 * @return the name
	 */

	public String getName() {
		return name;
	}

	/**
	 * Returns the List of stats for this item
	 * 
	 * @return the stats
	 */

	public List<StatNode> getStats() {
		return statModifications;
	}

	public ItemType getType() {
		return type;
	}

}
