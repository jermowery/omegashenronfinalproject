/**
 * 
 */
package view;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import model.GameState;
import model.ItemType;
import model.TileType;
import model.UnitType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class ImageFactory {

	private static ImageFactory instance;
	private HashMap<TileType, BufferedImage> tileImages;
	private HashMap<UnitType, BufferedImage> unitDefaultImages;
	private HashMap<UnitType, BufferedImage> animatedImages;
	private HashMap<ButtonType, Icon> enabledButtonImages;
	private HashMap<ButtonType, Icon> disabledButtonImages;
	private HashMap<ItemType, BufferedImage> itemImages;
	private HashMap<Boolean, BufferedImage> overlayImages;
	private BufferedImage splashScreen;

	/**
	 * 
	 */
	private ImageFactory() {
		tileImages = new HashMap<>();
		unitDefaultImages = new HashMap<>();
		animatedImages = new HashMap<>();
		enabledButtonImages = new HashMap<>();
		disabledButtonImages = new HashMap<>();
		itemImages = new HashMap<>();
		overlayImages = new HashMap<>();
		try {
			splashScreen = ImageIO.read(getClass().getResourceAsStream(
					"/splash_screen.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addImagesToHashMap();
	}

	private void addImagesToHashMap() {
		addTileImages();
		addUnitImages();
		addAnimatedImages();
		addEnabledButtonImages();
		addDisabledButtonImages();
		addItemImages();
		addOverLayImages();
	}

	private void addOverLayImages() {
		try {
			overlayImages.put(
					true,
					ImageIO.read(getClass().getResourceAsStream(
							"/action_overlay.png")));
			overlayImages.put(
					false,
					ImageIO.read(getClass().getResourceAsStream(
							"/enemy_overlay.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addDisabledButtonImages() {
		disabledButtonImages.put(ButtonType.ATTACK, new ImageIcon(getClass()
				.getResource("/attack_disabled.png")));
		disabledButtonImages.put(ButtonType.SPECIAL, new ImageIcon(getClass()
				.getResource("/special_attack_disabled.png")));
		disabledButtonImages.put(ButtonType.DO_NOTHING, new ImageIcon(
				getClass().getResource("/do_nothing_disabled.png")));
		disabledButtonImages.put(ButtonType.INVENTORY, new ImageIcon(getClass()
				.getResource("/inventory_disabled.png")));
		disabledButtonImages.put(ButtonType.SHOP, new ImageIcon(getClass()
				.getResource("/shop_disabled.png")));
		disabledButtonImages.put(ButtonType.TRADE, new ImageIcon(getClass()
				.getResource("/trade_disabled.png")));
		disabledButtonImages.put(ButtonType.MOVE, new ImageIcon(getClass()
				.getResource("/move_disabled.png")));
		disabledButtonImages.put(ButtonType.SPECIAL, new ImageIcon(getClass()
				.getResource("/special_attack_disabled.png")));
	}

	private void addAnimatedImages() {
		try {
			animatedImages.put(
					UnitType.ANNOYINGBRAT,
					ImageIO.read(getClass().getResourceAsStream(
							"/annoyingBrat_atk.png")));
			animatedImages.put(
					UnitType.GEARHEAD,
					ImageIO.read(getClass().getResourceAsStream(
							"/gearhead_atk.png")));
			animatedImages.put(
					UnitType.ROIDRAGEDIVER,
					ImageIO.read(getClass().getResourceAsStream(
							"/roidRageDiver_atk.png")));
			animatedImages.put(
					UnitType.TESLA,
					ImageIO.read(getClass().getResourceAsStream(
							"/tesla_atk.png")));
			animatedImages.put(
					UnitType.TROOPER,
					ImageIO.read(getClass().getResourceAsStream(
							"/trooper_atk.png")));
			animatedImages.put(
					UnitType.WEAVER,
					ImageIO.read(getClass().getResourceAsStream(
							"/weaver_atk.png")));
			animatedImages.put(
					UnitType.DRMERCER,
					ImageIO.read(getClass().getResourceAsStream(
							"/mercer_atk.png")));
			animatedImages.put(
					UnitType.DYLAN,
					ImageIO.read(getClass().getResourceAsStream(
							"/dylan_atk.png")));
			animatedImages.put(UnitType.ERIC, ImageIO.read(getClass()
					.getResourceAsStream("/eric_atk.png")));
			animatedImages.put(
					UnitType.COMMANDER,
					ImageIO.read(getClass().getResourceAsStream(
							"/commander_atk.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addItemImages() {
		try {
			itemImages.put(ItemType.BIGGER_CANNON,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.CANNON,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.DRAGONBALLS,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.GRADE_A_HEALTH_TONIC,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.GRADE_B_HEALTH_TONIC,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.GRADE_S_HEALTH_TONIC,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.FOUNTAINHEAD,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.HARPOON,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.HEAL,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.KOOL_AID,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.LEAKY_PEN,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.MORAL_BOOST,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.POLO_CLUB_ROGUE,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.RAD_BIKER_GLASSES,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.SCARY_SPIDERS,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.SHARK_CLOAK,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.SHAVING_RAZOR,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.SIRGEOS_STRENGTH_SHAMPOO,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.SKATE_SHOES,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.TALKING_SWORD,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.THE_OVERCOMPENSATOR,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.WIZARDS_DIARY,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
			itemImages.put(ItemType.RAGE_BREW,
					ImageIO.read(getClass().getResourceAsStream("/item.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Could not load item images into the hashmap.");
		}

	}

	private void addEnabledButtonImages() {
		enabledButtonImages.put(ButtonType.ATTACK, new ImageIcon(getClass()
				.getResource("/attack.png")));
		enabledButtonImages.put(ButtonType.SPECIAL, new ImageIcon(getClass()
				.getResource("/special_attack.png")));
		enabledButtonImages.put(ButtonType.DO_NOTHING, new ImageIcon(getClass()
				.getResource("/do_nothing.png")));
		enabledButtonImages.put(ButtonType.INVENTORY, new ImageIcon(getClass()
				.getResource("/inventory.png")));
		enabledButtonImages.put(ButtonType.SHOP, new ImageIcon(getClass()
				.getResource("/shop.png")));
		enabledButtonImages.put(ButtonType.TRADE, new ImageIcon(getClass()
				.getResource("/trade.png")));
		enabledButtonImages.put(ButtonType.MOVE, new ImageIcon(getClass()
				.getResource("/move.png")));
		enabledButtonImages.put(ButtonType.SPECIAL, new ImageIcon(getClass()
				.getResource("/special_attack.png")));

	}

	private void addUnitImages() {
		try {
			unitDefaultImages.put(
					UnitType.ANNOYINGBRAT,
					ImageIO.read(getClass().getResourceAsStream(
							"/annoyingBrat_idle.png")));
			unitDefaultImages.put(
					UnitType.GEARHEAD,
					ImageIO.read(getClass().getResourceAsStream(
							"/gearhead_idle.png")));
			unitDefaultImages.put(
					UnitType.ROIDRAGEDIVER,
					ImageIO.read(getClass().getResourceAsStream(
							"/roidRageDiver_idle.png")));
			unitDefaultImages.put(
					UnitType.TESLA,
					ImageIO.read(getClass().getResourceAsStream(
							"/tesla_idle.png")));
			unitDefaultImages.put(
					UnitType.TROOPER,
					ImageIO.read(getClass().getResourceAsStream(
							"/trooper_idle.png")));
			unitDefaultImages.put(
					UnitType.WEAVER,
					ImageIO.read(getClass().getResourceAsStream(
							"/weaver_idle.png")));
			unitDefaultImages.put(
					UnitType.DRMERCER,
					ImageIO.read(getClass().getResourceAsStream(
							"/mercer_idle.png")));
			unitDefaultImages.put(
					UnitType.DYLAN,
					ImageIO.read(getClass().getResourceAsStream(
							"/dylan_idle.png")));
			unitDefaultImages.put(
					UnitType.ERIC,
					ImageIO.read(getClass().getResourceAsStream(
							"/eric_idle.png")));
			unitDefaultImages.put(
					UnitType.COMMANDER,
					ImageIO.read(getClass().getResourceAsStream(
							"/commander_idle.png")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Could not load idle unit images.");
		}

	}

	public void addTileImages() {
		try {
			tileImages.put(TileType.DESERT,
					ImageIO.read(getClass().getResourceAsStream("/dirt.png")));
			tileImages.put(TileType.WATER,
					ImageIO.read(getClass().getResourceAsStream("/water.png")));
			tileImages.put(TileType.WALL,
					ImageIO.read(getClass().getResourceAsStream("/wall.png")));
			// tileImages.put(TileType.ITEM,
			// ImageIO.read(getClass().getResourceAsStream("/item.png")));
			tileImages
					.put(TileType.RUBBLE,
							ImageIO.read(getClass().getResourceAsStream(
									"/rubble.png")));
			tileImages.put(
					TileType.TESLA_COIL,
					ImageIO.read(getClass().getResourceAsStream(
							"/tesla_coil.png")));
		} catch (IOException e) {
			System.out.println("Could not load tile images.");
		}

	}

	public static ImageFactory getInstance() {
		if (instance == null) {
			instance = new ImageFactory();
		}
		return instance;
	}

	public BufferedImage getTerrainImage(TileType tileType) {
		return tileImages.get(tileType);
	}

	public BufferedImage getUnitDefaultImage(UnitType type) {
		return unitDefaultImages.get(type);
	}

	public BufferedImage getItemImage(ItemType type) {
		return itemImages.get(type);
	}

	public BufferedImage getOverlay(GameState state) {
		return overlayImages.get(true);
	}

	public BufferedImage getAIOverlay() {
		return overlayImages.get(false);
	}

	public Icon getEnabledImage(ButtonType buttonType) {
		return enabledButtonImages.get(buttonType);
	}

	public Icon getDisabledImage(ButtonType buttonType) {
		return disabledButtonImages.get(buttonType);
	}

	public BufferedImage getUnitAttackImage(UnitType type) {
		return animatedImages.get(type);
	}

	public BufferedImage getSplashScren() {
		return splashScreen;
	}
}
