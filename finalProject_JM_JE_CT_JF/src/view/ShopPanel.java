/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import model.Game;
import model.Item;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         This is the panel shown for the shop
 */
@SuppressWarnings("serial")
public class ShopPanel extends JPanel {

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class ShopTableListener implements ListSelectionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * javax.swing.event.ListSelectionListener#valueChanged(javax.swing.
		 * event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			select.setEnabled(true);
			int row = shopTable.getSelectedRow();
			Item selectedItem = list.get(row);
			imagePanel.setImage(ImageFactory.getInstance().getItemImage(
					selectedItem.getType()));
			imagePanel.repaint();
			System.out.println("Hello from the table");
		}

	}

	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	private class SelectActionListener implements ActionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			int row = shopTable.getSelectedRow();
			Item selectedItem = list.get(row);
			if (Game.getInstance().getCurrentPlayer()
					.canBuy(selectedItem.getPrice())) {
				Game.getInstance().purchaseItem(selectedItem);
			} else {
				JOptionPane.showMessageDialog(null,
						"You cannot buy this, Ima bust a cap");
				Game.getInstance().closeTheStore();
			}
			select.setEnabled(false);

		}

	}

	JButton select;
	JTable shopTable;
	ItemImagePanel imagePanel;
	List<Item> list;

	/**
	 * Use this constructor to start the panel with elements
	 */
	public ShopPanel(List<Item> list) {
		super();
		this.list = list;
		select = new JButton("Select");
		select.setEnabled(false);
		ShopTableModel model = new ShopTableModel(list);
		shopTable = new JTable(model);
		shopTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		imagePanel = new ItemImagePanel();
		select.addActionListener(new SelectActionListener());
		shopTable.getSelectionModel().addListSelectionListener(
				new ShopTableListener());

		this.setLayout(new BorderLayout());
		imagePanel.setPreferredSize(new Dimension(50, 200));
		this.add(imagePanel, BorderLayout.WEST);
		shopTable.setPreferredSize(new Dimension(700, 400));
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(
				model);
		shopTable.setRowSorter(sorter);
		this.add(shopTable, BorderLayout.EAST);

		select.setPreferredSize(new Dimension(50, 100));
		this.add(select, BorderLayout.SOUTH);
	}

	/**
	 * @param layout
	 */
	public ShopPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public ShopPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public ShopPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	public void setList(List<Item> storeItems) {
		this.remove(shopTable);
		this.list = storeItems;
		shopTable = new JTable(new ShopTableModel(list));
		shopTable.getSelectionModel().addListSelectionListener(
				new ShopTableListener());
		this.add(shopTable, 1);
		revalidate();
	}

}
