/**
 * 
 */
package view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class ItemImagePanel extends JPanel {

	private BufferedImage itemImage;

	public void setImage(BufferedImage itemImage) {
		this.itemImage = itemImage;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println("Got to paint component");
		Graphics2D g2 = (Graphics2D) g;
		System.out.println(itemImage);
		if (itemImage != null) {
			System.out.println("I have an image!");
			g2.drawImage(itemImage, 0, 0, null);
		}
	}
}
