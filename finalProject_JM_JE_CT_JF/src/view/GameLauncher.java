/**
 * 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.AnnoyingBrat;
import model.Game;
import model.Gearhead;
import model.MapType;
import model.Player;
import model.RoidRageDiver;
import model.ScenarioType;
import model.Team;
import model.Tesla;
import model.Trooper;
import model.Unit;
import model.UnitStrategy;
import model.Weaver;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class GameLauncher extends JFrame {
	/**
	 * @author Jeremy Mowery
	 * @author Jacob Fritts
	 * @author Jamel El-Merhaby
	 * @author Cindy Trieu
	 */
	@SuppressWarnings("serial")
	private class BackgrondPanel extends JPanel {
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			BufferedImage backgroundImage = ImageFactory.getInstance()
					.getSplashScren();
			g2.drawImage(backgroundImage, 0, 0, null);
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel launcherOptionsPanel;
	@SuppressWarnings("unused")
	private Game game;
	private GameGUI gui;
	private JPanel gameOptionsPanel;
	private JPanel backgroundPanel;

	/**
	 * launches the game launcher
	 */
	public GameLauncher() {

		launcherOptionsPanel = new LauncherOptionsPanel();
		gameOptionsPanel = new GameOptionsPanel();
		gameOptionsPanel.setSize(new Dimension(850, 450));
		setJFrameProperties();
		launcherOptionsPanel.setSize(new Dimension(150, 200));
		launcherOptionsPanel.setOpaque(true);
		this.setLayout(new BorderLayout());

		backgroundPanel = new BackgrondPanel();
		backgroundPanel.setLayout(new BorderLayout());

		backgroundPanel.add(launcherOptionsPanel, BorderLayout.EAST);
		backgroundPanel.setPreferredSize(new Dimension(850, 450));
		this.add(backgroundPanel, BorderLayout.CENTER);

	}

	private void setJFrameProperties() {
		this.setTitle("Hoovervillez Launcher");
		this.setSize(850, 450);
		this.setResizable(false);
		this.setLayout(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				GameLauncher launcher = new GameLauncher();
				launcher.setVisible(true);

			}
		});

	}

	@SuppressWarnings("serial")
	private class LauncherOptionsPanel extends JPanel {

		private JButton newGame;
		private JButton loadGame;
		private JButton aboutGame;

		/**
		 * 
		 */
		public LauncherOptionsPanel() {
			initializeButtons();
			this.setPreferredSize(new Dimension(120, 450));
			this.setLayout(new GridLayout(5, 1, 50, 50));
			this.add(new JLabel());
			this.add(newGame);
			this.add(loadGame);
			this.add(aboutGame);
			this.setVisible(true);
			this.setBackground(new Color(216, 184, 153));
			System.out.println("hello");
		}

		private void initializeButtons() {
			newGame = new JButton("New Game");
			loadGame = new JButton("Load Game");
			aboutGame = new JButton("About Game");

			setButtonSizes();
			setButtonLocations();
			addButtonsToScreen();
			addButtonListeners();

		}

		private void setButtonLocations() {
			newGame.setLocation(235, 150);
			loadGame.setLocation(235, 180);
			aboutGame.setLocation(235, 210);

		}

		private void setButtonSizes() {
			newGame.setSize(150, 25);
			loadGame.setSize(150, 25);
			aboutGame.setSize(150, 25);
		}

		private void addButtonsToScreen() {
			add(newGame);
			add(loadGame);
			add(aboutGame);
		}

		/**
		 * Attaches button listeners to the buttons
		 */
		private void addButtonListeners() {
			newGame.addActionListener(new NewGameListener());
			loadGame.addActionListener(new LoadGameListener());
			aboutGame.addActionListener(new AboutGameListener());

		}

		private class NewGameListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent event) {
				GameLauncher.this.backgroundPanel.remove(launcherOptionsPanel);
				GameLauncher.this.backgroundPanel.add(gameOptionsPanel,
						BorderLayout.CENTER);
				GameLauncher.this.backgroundPanel.validate();
				JOptionPane.showMessageDialog(null, "Please select "
						+ UnitStrategy.MAX_NUMBER_OF_UNITS + " units");

			}
		}

		// private class SaveButtonListener implements ActionListener {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// File saveGame = new File("resources/savedGame.txt");
		// try {
		// FileOutputStream outputStream = new FileOutputStream(
		// saveGame);
		// ObjectOutputStream objectOut = new ObjectOutputStream(
		// outputStream);
		// objectOut.writeObject(gui);
		// objectOut.close();
		// outputStream.close();
		//
		// } catch (FileNotFoundException e1) {
		// System.out.println("Error 000x091693: File not found.");
		// } catch (IOException e1) {
		// System.out
		// .println("Error 000x120313: Game could not be saved.");
		// }
		//
		// }
		//
		// }

		private class LoadGameListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				FileInputStream fileIn;
				try {
					fileIn = new FileInputStream(new File(
							"resources/savedGame.txt"));
					ObjectInputStream objectIn = new ObjectInputStream(fileIn);
					Game.setInstance((Game) objectIn.readObject());
					gui = new GameGUI();
					gui.setVisible(true);
					System.out.println("loaded");
					objectIn.close();
					fileIn.close();
					GameLauncher.this.setVisible(false);
					GameLauncher.this.dispose();

				} catch (FileNotFoundException a) {
					System.out.println("Error 000x091693: File not found.");
				} catch (IOException b) {
					System.out
							.println("Error 000x120313: Game could not be loaded.");
				} catch (ClassNotFoundException c) {
					System.out
							.println("Error 000x072012: Class could not be found.");
				}

			}

		}

		private class AboutGameListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				String message = "Game created by:\nJamel \"Jamaymay\" El-Merhaby\nJake \"The DJ of love\" Fritts\nJeremy \"Mr. Awsome\" Mowery\nCindy !false";

				JOptionPane.showMessageDialog(null, message);

			}

		}
	}

	@SuppressWarnings("serial")
	private class GameOptionsPanel extends JPanel implements Serializable {

		private JButton start;

		private JComboBox<String> mapSelection;

		private JComboBox<String> scenarioSelection;
		@SuppressWarnings("rawtypes")
		private HashMap<String, Enum> gameOptions;

		private JPanel gameConfig;
		private JPanel unitConfig;

		private JLabel mapLabel;

		private JLabel scenarioLabel;

		private JLabel weaverLabel;
		private JLabel trooperLabel;
		private JLabel teslaLabel;
		private JLabel gearHeadLabel;
		private JLabel annoyingBratLabel;
		private JLabel roidRageDiverLabel;
		// private JLabel commanderLabel;

		private JComboBox<Integer> weaverCount;
		private JComboBox<Integer> trooperCount;
		private JComboBox<Integer> teslaCount;
		private JComboBox<Integer> gearHeadCount;
		private JComboBox<Integer> annoyingBratCount;
		private JComboBox<Integer> roidRageDiverCount;

		private Game game;

		/**
		 * Creates a game GUI
		 */
		public GameOptionsPanel() {
			setupHashMap();
			initializeComboBoxes();
			initializeButtons();
			initializeLabels();
			initializePanels();
			setComponentSizes();
			addComponentsToScreen();

		}

		private void setComponentSizes() {
			start.setSize(75, 25);
			gameConfig.setPreferredSize(new Dimension(800, 150));
			unitConfig.setPreferredSize(new Dimension(800, 150));

		}

		private void initializePanels() {
			gameConfig = new JPanel();
			unitConfig = new JPanel();

		}

		private void initializeLabels() {

			mapLabel = new JLabel("Select map:");

			scenarioLabel = new JLabel("Select scenario:");

			weaverLabel = new JLabel("Weaver");
			trooperLabel = new JLabel("Trooper");
			teslaLabel = new JLabel("Tesla");
			gearHeadLabel = new JLabel("Gearhead");
			annoyingBratLabel = new JLabel("Annoying Brat");
			roidRageDiverLabel = new JLabel("Roid Rage Diver");
		}

		private void addComponentsToScreen() {
			add(gameConfig);
			add(unitConfig);
			addComponentsToPanel();

		}

		private void addComponentsToPanel() {
			addToGameConfigPanel();
			addToUnitPanel();
		}

		private void addToUnitPanel() {
			unitConfig.add(weaverLabel);
			unitConfig.add(weaverCount);
			unitConfig.add(trooperLabel);
			unitConfig.add(trooperCount);
			unitConfig.add(teslaLabel);
			unitConfig.add(teslaCount);
			unitConfig.add(gearHeadLabel);
			unitConfig.add(gearHeadCount);
			unitConfig.add(annoyingBratLabel);
			unitConfig.add(annoyingBratCount);
			unitConfig.add(roidRageDiverLabel);
			unitConfig.add(roidRageDiverCount);
			unitConfig.add(start);
		}

		private void addToGameConfigPanel() {
			gameConfig.add(mapLabel);
			gameConfig.add(mapSelection);

			gameConfig.add(scenarioLabel);
			gameConfig.add(scenarioSelection);
		}

		private void setupHashMap() {
			gameOptions = new HashMap<>();

			setUpGameConfigHashMap();
		}

		private void setUpGameConfigHashMap() {
			// objectives
			addObjectivesToMap();
			// map type
			addMapTypesToMap();
		}

		private void addMapTypesToMap() {
			gameOptions.put("Easy", MapType.EASY);
			gameOptions.put("Medium", MapType.MEDIUM);
			gameOptions.put("Hard", MapType.HARD);
			gameOptions.put("Random", MapType.RANDOM);
		}

		private void addObjectivesToMap() {
			gameOptions.put("Collect Dragon Ballz",
					ScenarioType.BUY_DRAGONBALLS);
			gameOptions.put("Assassination", ScenarioType.KILL_COMMANDER);
			gameOptions.put("Escape", ScenarioType.ESCAPE);
		}

		private void initializeComboBoxes() {
			initializeGameConfigOptions();
			initializeUnitConfigOptions();
			addItemsToComboBoxes();
		}

		private void initializeUnitConfigOptions() {
			weaverCount = new JComboBox<>();
			trooperCount = new JComboBox<>();
			teslaCount = new JComboBox<>();
			gearHeadCount = new JComboBox<>();
			annoyingBratCount = new JComboBox<>();
			roidRageDiverCount = new JComboBox<>();
		}

		private void initializeGameConfigOptions() {
			mapSelection = new JComboBox<>();
			scenarioSelection = new JComboBox<>();
		}

		private void addItemsToComboBoxes() {
			addMapTypes();
			addScenarioTypes();
			addUnitOptions();
		}

		private void addUnitOptions() {
			for (int index = 0; index <= UnitStrategy.MAX_NUMBER_OF_UNITS; index++) {
				weaverCount.addItem(index);
				trooperCount.addItem(index);
				teslaCount.addItem(index);
				gearHeadCount.addItem(index);
				annoyingBratCount.addItem(index);
				roidRageDiverCount.addItem(index);
			}

		}

		private void addScenarioTypes() {
			scenarioSelection.addItem("Collect Dragon Ballz");
			scenarioSelection.addItem("Assassination");
			scenarioSelection.addItem("Escape");
		}

		private void addMapTypes() {
			mapSelection.addItem("Easy");
			mapSelection.addItem("Medium");
			mapSelection.addItem("Hard");
			mapSelection.addItem("Random");
		}

		private void initializeButtons() {
			start = new JButton("Start");

			addActionListenersToButtons();
		}

		private void addActionListenersToButtons() {
			start.addActionListener(new StartButtonListener());

		}

		// public void saveGame() {
		// File saveGame = new File("resources/savedGame.txt");
		// try {
		// FileOutputStream outputStream = new FileOutputStream(saveGame);
		// ObjectOutputStream objectOut = new ObjectOutputStream(
		// outputStream);
		// objectOut.writeObject(this);
		// objectOut.close();
		// outputStream.close();
		//
		// } catch (FileNotFoundException e1) {
		// System.out.println("Error 000x091693: File not found.");
		// } catch (IOException e1) {
		// System.out
		// .println("Error 000x120313: Game could not be saved.");
		// }
		// }

		private class StartButtonListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent event) {
				// Determine if the user selects are valid
				int numWeaver = (int) weaverCount.getSelectedItem();
				int numTrooper = (int) trooperCount.getSelectedItem();
				int numTesla = (int) teslaCount.getSelectedItem();
				int numGearHead = (int) gearHeadCount.getSelectedItem();
				int numAnnoyingBrat = (int) annoyingBratCount.getSelectedItem();
				int numRoidRageDiver = (int) roidRageDiverCount
						.getSelectedItem();
				if (numWeaver + numTrooper + numTesla + numRoidRageDiver
						+ numGearHead + numAnnoyingBrat != UnitStrategy.MAX_NUMBER_OF_UNITS) {
					JOptionPane.showMessageDialog(null, "You must select "
							+ UnitStrategy.MAX_NUMBER_OF_UNITS + "units!");
				} else {
					game = Game.getInstance();
					// System.out.println(gameOptions.get(mapSelection
					// .getSelectedItem().toString()));
					// System.out.println(gameOptions.get(scenarioSelection
					// .getSelectedItem().toString()));

					// Make the units for this palyer
					List<Unit> units = new ArrayList<>();
					for (int unitNum = 0; unitNum < numWeaver; unitNum++) {
						Weaver temp = new Weaver();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					for (int unitNum = 0; unitNum < numTrooper; unitNum++) {
						Trooper temp = new Trooper();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					for (int unitNum = 0; unitNum < numTesla; unitNum++) {
						Tesla temp = new Tesla();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					for (int unitNum = 0; unitNum < numGearHead; unitNum++) {
						Gearhead temp = new Gearhead();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					for (int unitNum = 0; unitNum < numAnnoyingBrat; unitNum++) {
						AnnoyingBrat temp = new AnnoyingBrat();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					for (int unitNum = 0; unitNum < numRoidRageDiver; unitNum++) {
						RoidRageDiver temp = new RoidRageDiver();
						temp.setTeam(Team.PLAYER);
						units.add(temp);
					}
					Player player = new Player();
					player.setUnits(units);
					ScenarioType scenarioType = (ScenarioType) gameOptions
							.get(scenarioSelection.getSelectedItem().toString());
					MapType mapType = (MapType) gameOptions.get(mapSelection
							.getSelectedItem().toString());
					game.setScenario(scenarioType);
					game.setPlayer(player);
					game.setMap(mapType);
					gui = new GameGUI();
					gui.setVisible(true);
					GameLauncher.this.setVisible(false);
					GameLauncher.this.dispose();
				}
			}
		}

	}

}
