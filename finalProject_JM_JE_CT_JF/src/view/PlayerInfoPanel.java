/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.Game;
import model.ScenarioStrategy;
import model.ScenarioType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         Panel for the player info: a button for the objectives, the turn
 *         count, and money
 */
@SuppressWarnings("serial")
public class PlayerInfoPanel extends JPanel {

	public class EndTurnListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Game.getInstance().endTurn();

		}

	}

	private class ObjectivesButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null, objectivesString);

		}

	}

	private JLabel money;
	private JLabel turnCount;
	private JButton objectives;
	private JButton endTurn;
	private String objectivesString;

	private JPanel accessPanel;

	/**
	 * Use this constructor to start the panel off with everything it needs
	 */
	public PlayerInfoPanel() {
		initializeLabels();
		initializeButtons();
		initializePanels();
		addComponentsToScreen();
		this.setPreferredSize(new Dimension(800, 75));
	}

	private void initializePanels() {
		accessPanel = new JPanel();

	}

	private void addComponentsToScreen() {
		addComponentsToPanel();

	}

	private void addComponentsToPanel() {
		accessPanel.add(money);
		accessPanel.add(objectives);
		accessPanel.add(turnCount);
		accessPanel.add(endTurn);

	}

	/**
	 * 
	 */
	private void initializeLabels() {
		money = new JLabel("Money: $"
				+ Game.getInstance().getCurrentPlayer().getMoney());
		turnCount = new JLabel("Turn count: "
				+ Game.getInstance().getTurnCount());
	}

	/**
	 * 
	 */
	private void initializeButtons() {
		endTurn = new JButton("End Turn");
		objectives = new JButton("Objectives");

		attachActionListeners();
	}

	/**
	 * 
	 */
	private void attachActionListeners() {
		objectives.addActionListener(new ObjectivesButtonListener());
		endTurn.addActionListener(new EndTurnListener());
	}

	/**
	 * @param arg0
	 */
	public PlayerInfoPanel(LayoutManager arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public PlayerInfoPanel(boolean arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public PlayerInfoPanel(LayoutManager arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Sets the objectives for the scenarios passed in
	 * 
	 * @param scenarioTypes
	 *            the scenarios to create objectives for
	 */
	public void setObjectives(ScenarioType scenarioTypes) {
		objectivesString = ScenarioStrategy
				.getScenarioAsString(ScenarioType.KILL_ALL_ENEMIES) + "\n";
		objectivesString += ScenarioStrategy.getScenarioAsString(scenarioTypes);
	}

	/**
	 * Changes the money display the the money passed in
	 * 
	 * @param money
	 *            the money to change the display to
	 */
	public void setMoney(double money) {
		this.money.setText("Money: "
				+ Game.getInstance().getCurrentPlayer().getMoney());
	}

	/**
	 * Changes the display of the turn count to whatever is passed in
	 * 
	 * @param turnCount
	 */
	public void setTurns(int turnCount) {
		this.turnCount.setText("Turn count: "
				+ Game.getInstance().getTurnCount());
	}

}
