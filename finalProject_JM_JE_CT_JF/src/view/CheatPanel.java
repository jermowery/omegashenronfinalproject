/**
 * 
 */
package view;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Game;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
@SuppressWarnings("serial")
public class CheatPanel extends JPanel {

	JTextField cheatText;
	JLabel cheatLabel;

	/**
	 * Use this constructor to start the panel off with the elements
	 */
	public CheatPanel() {

		super();
		this.setPreferredSize(new Dimension(305, 225));
		cheatText = new JTextField();
		cheatLabel = new JLabel("  Enter cheats! ");
		cheatText.setPreferredSize(new Dimension(200, 50));

		cheatText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				System.out.println("Cheated");
				String cheatString = cheatText.getText();
				Game.getInstance().cheat(cheatString);
			}
		});
		this.add(cheatLabel);
		this.add(cheatText);

	}

	/**
	 * @param layout
	 */
	public CheatPanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param isDoubleBuffered
	 */
	public CheatPanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param layout
	 * @param isDoubleBuffered
	 */
	public CheatPanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}

}
