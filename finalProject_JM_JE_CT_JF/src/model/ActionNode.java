/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 * 
 *         Defines a action node representing the name of an action
 */
public class ActionNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The name of the action
	 */
	private String actionName;
	/**
	 * The type of the action as defined by the enum
	 */
	private ActionType action;

	private boolean isDisabled;

	private int actionPointsToPerform;

	/**
	 * Constructs an ActionNode
	 * 
	 * @param actionName
	 *            the name of the action
	 * @param action
	 *            the action
	 * @param actionPointsToPerform
	 *            TODO
	 */
	public ActionNode(String actionName, ActionType action,
			int actionPointsToPerform) {
		this.actionName = actionName;
		this.action = action;
		this.isDisabled = false;
		this.actionPointsToPerform = actionPointsToPerform;
	}

	public ActionType getAction() {
		return action;
	}

	public String getActionName() {
		return actionName;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public int getActionPointsToPerform() {
		return actionPointsToPerform;
	}

}
