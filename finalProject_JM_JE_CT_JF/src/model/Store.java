/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 */
@SuppressWarnings("serial")
public class Store implements Serializable {
	private List<Item> allItems;
	private Inventory shuffledItems;
	private final int NUMBER_OF_ITEMS = 3;

	/**
	 * Constructor creates a reference to a list of all items and calls the
	 * shuffleItems method
	 */

	public Store() {
		allItems = ItemStrategy.allItems();
		shuffledItems = new Inventory();
		shuffleItems();
	}

	/**
	 * Shuffles all the items and then adds the first five to an Inventory
	 * object
	 */

	public void shuffleItems() {
		Collections.shuffle(allItems);
		shuffledItems = new Inventory();
		for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
			shuffledItems.add(((Item) allItems.get(i)));
		}
	}

	/**
	 * 
	 * @return the Inventory of the store
	 */

	public Inventory getItems() {
		return shuffledItems;
	}

	/**
	 * Takes in an item and checks whether or not the item is available
	 * currently in the store. If the item is in the store, it returns the
	 * item's price. Otherwise, it will return -1.
	 * 
	 * @param item
	 * @return
	 */

	public int getPrice(int index) {
		return shuffledItems.get(index).getPrice();
	}

	/**
	 * The toString will return the String representation of the items shown in
	 * the store
	 */

	public String toString() {
		String result = "";

		for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
			result += shuffledItems.get(i).getName() + ": $"
					+ shuffledItems.get(i).getPrice() + "\n";
		}

		return result;
	}

}
