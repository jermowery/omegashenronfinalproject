/**
 * 
 */
package view;

import model.UnitType;

/**
 * @author Jeremy Mowery
 * @author Jacob Fritts
 * @author Jamel El-Merhaby
 * @author Cindy Trieu
 */
public class AnimationPackage {

	UnitType unitType;
	AnimationState animationState;

	/**
	 * 
	 */
	public AnimationPackage(UnitType unitType, AnimationState animationState) {
		this.unitType = unitType;
		this.animationState = animationState;
	}

}
