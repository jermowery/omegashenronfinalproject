/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

import tiles.Rubble;
import tiles.TeslaCoil;

/**
 * @author Jamel El-Merhaby
 * @author Jake Fritts
 * @author Jeremy Mowery
 * @author Cindy Trieu
 * 
 *         This class is now a singleton to have lower coupling
 */
public class Game extends Observable implements Serializable {

	private MapType mapType;
	/**
	 * A reference to the state of the game;
	 */
	private GameState state;

	private static Game instance;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * currentPlayer is the player playing the game.
	 */
	private Player currentPlayer;
	/**
	 * map is the world.
	 */
	private Map map;

	/**
	 * store is the in-game store.
	 */
	private Store store;

	/**
	 * Scenario is ???.
	 */

	/**
	 * ai represents the enemy AI determined by game conditions.
	 */
	private AI AI;

	/**
	 * turnCount keeps track of the number of turns that has elapsed since the
	 * start of a new game.
	 */
	private int turnCount;

	public int getTurnCount() {
		return turnCount;
	}

	/**
	 * selectedTile is the tile currently selected.
	 */
	private Tile selectedTile;

	public Tile getSelectedTile() {
		return selectedTile;
	}

	/**
	 * isGameOver returns true if the game has ended otherwise it is false.
	 */
	private boolean isGameOver;

	/**
	 * selectedUnitAction is the action that a unit has been ordered to take.
	 */
	private ActionType selectedUnitAction;

	public ActionType getSelectedUnitAction() {
		return selectedUnitAction;
	}

	/**
	 * tileActions represents a list of possible actions that the user can take
	 * based on the tile it is on.
	 */
	private List<ActionNode> tileActions;

	public List<ActionNode> getTileActions() {
		return tileActions;
	}

	/**
	 * Prepares the game for loading by reseting the state of and the selection
	 */
	public void loadGame() {
		resetSelection();
		state = GameState.SELECTING_UNIT;
		setChanged();
		notifyObservers();
	}

	/**
	 * tilesToApplyAction keeps track of the tiles affected by the units action.
	 */
	private List<Tile> tilesToApplyAction;

	public List<Tile> getTilesToApplyAction() {
		return tilesToApplyAction;
	}

	/**
	 * keeps track of the current game scenario.
	 */
	private Scenario gameScenario;

	@SuppressWarnings("unused")
	private boolean storeIsOpen = false;
	private boolean inventoryIsOpen = false;
	private AttackMediator attackMediator;
	private SpecialMediator specialMediator;
	private ScenarioType scenarioType;

	public ScenarioType getScenarioType() {
		return scenarioType;
	}

	public static void setInstance(Game game) {
		Game.instance = game;
	}

	// This is just the beginning of a constructor.

	public void setScenario(ScenarioType typeOfScenario) {
		scenarioType = typeOfScenario;
		gameScenario = new Scenario(typeOfScenario);
		AI = new AI();
	}

	public void setPlayer(Player player) {
		currentPlayer = player;

	}

	public void setMap(MapType typeOfMap) {
		mapType = typeOfMap;
		map = new Map(typeOfMap, currentPlayer.getUnits(), AI.getUnits());
	}

	private Game() {
		store = new Store();
		turnCount = 0;
		selectedTile = null;
		isGameOver = false;
		selectedUnitAction = null;
		tileActions = null;
		tilesToApplyAction = null;
		attackMediator = new AttackMediator();
		specialMediator = new SpecialMediator();
		AI = new AI();
		state = GameState.SELECTING_UNIT;
	}

	public static Game getInstance() {
		if (instance == null) {
			instance = new Game();
		}
		return instance;
	}

	public GameState getState() {
		return state;
	}

	public void resetGame() {
		instance = new Game();
		currentPlayer = null;
		map = null;
		scenarioType = null;
		gameScenario = null;
		state = GameState.NEW_GAME;
		setChanged();
		notifyObservers();
	}

	public void trade(Unit unitTradingFrom, Unit unitTradingTo, Item itemToTrade) {
		unitTradingFrom.remove(itemToTrade);
		unitTradingTo.addToInventory(itemToTrade);
		inventoryIsOpen = false;
		resetSelection();
		state = GameState.EXITING_INVENTORY;
		setChanged();
		notifyObservers();
	}

	public boolean isGameOver() {
		return isGameOver;
	}

	public void setCurrentPlayer(Player player) {
		currentPlayer = player;
	}

	/**
	 * Enables cheating, a cheat string is passed in and if it is valid the
	 * proper thing happens
	 * 
	 * @param cheatString
	 *            the string entered
	 */
	public void cheat(String cheatString) {
		Unit temp;
		List<Unit> playerUnits = currentPlayer.getUnits();
		switch (cheatString.toLowerCase()) {
		case CheatStrategy.ERIC_STRING:
			temp = new Eric();
			temp.setTeam(Team.PLAYER);
			playerUnits.add(temp);
			currentPlayer.setUnits(playerUnits);
			placeUnitRandomly(temp);
			break;
		case CheatStrategy.DYLAN_STRING:
			temp = new Dylan();
			temp.setTeam(Team.PLAYER);
			playerUnits.add(temp);
			currentPlayer.setUnits(playerUnits);
			placeUnitRandomly(temp);
			break;
		case CheatStrategy.MERCER_STRING:
			temp = new DrMercer();
			temp.setTeam(Team.PLAYER);
			playerUnits.add(temp);
			currentPlayer.setUnits(playerUnits);
			placeUnitRandomly(temp);
		default:
			break;
		}
		resetSelection();
		state = GameState.CHEATED;
		setChanged();
		notifyObservers();
	}

	/**
	 * PLaces a unit randomly on the map, if 10000 attempts are made without any
	 * luck this method exits
	 * 
	 * @param unit
	 *            the unit to place
	 */
	private void placeUnitRandomly(Unit unit) {
		Random numGen = new Random();
		int row = numGen.nextInt(map.getRows());
		int col = numGen.nextInt(map.getCols());
		int i = 0;
		while (map.getTile(row, col).getCurrentUnit() != null) {
			if (i == 10000) {
				return;
			}
			row = numGen.nextInt(map.getRows());
			col = numGen.nextInt(map.getCols());
			i++;

		}

		map.placeUnit(unit, row, col);
		System.out.println("Row: " + row);
		System.out.println("Col: " + col);
	}

	public List<Item> getStoreItems() {
		return store.getItems().getAllItems();
	}

	public void selectUnit(int row, int col) {
		tileActions = null;
		selectedTile = map.getTile(row, col);
		if (selectedTile.getCurrentUnit() != null) {
			state = GameState.SELECTED_UNIT;
			if (selectedTile.getCurrentUnit().getTeam() == Team.PLAYER) {
				tileActions = selectedTile.getTileActions();
			}
		} else {
			state = GameState.SELECTED_TILE;
		}

		if (tileActions != null) {
			for (int i = 0; i < tileActions.size(); i++) {
				if (selectedTile.getCurrentUnit().getStat(
						StatType.ACTION_POINTS) < tileActions.get(i)
						.getActionPointsToPerform()) {
					tileActions.get(i).setDisabled(true);
					System.out.println("Setting disabled");
				} else {
					tileActions.get(i).setDisabled(false);
					System.out.println("Setting enabled");
				}
			}
		}
		setChanged();
		notifyObservers();
	}

	public void selectUnitAction(ActionType action) {
		selectedUnitAction = action;

		switch (action) {
		case SPECIAL:
			state = GameState.SPECIAL;
			// setting tilesToApplyAction as the units first, will modify based
			// on unit selected
			map.setActionType(ActionType.SPECIAL);
			tilesToApplyAction = map.getUnitsInRange(selectedTile
					.getCurrentUnit());
			alterTilesToApplyAction(Team.PLAYER);
			break;
		case ATTACK:
			state = GameState.ATTACKING;
			map.setActionType(ActionType.ATTACK);
			tilesToApplyAction = map.getUnitsInRange(selectedTile
					.getCurrentUnit());
			alterTilesToApplyAction(Team.AI);
			break;
		case MOVE:
			state = GameState.MOVING;
			map.setActionType(ActionType.MOVE);
			tilesToApplyAction = map.getPossibleMovementTiles(selectedTile
					.getCurrentUnit());
			break;
		case SHOP:
			state = GameState.SHOPPING;
			storeIsOpen = true;
			break;
		case INVENTORY:
			state = GameState.INVENTORY;
			inventoryIsOpen = true;
			break;
		case NOTHING:
			state = GameState.SELECTING_UNIT;
			break;
		case TRADE:
			state = GameState.TRADING;
			map.setActionType(ActionType.TRADE);
			tilesToApplyAction = map.getFriendlyUnitsInRange(selectedTile
					.getCurrentUnit());
			break;
		default:
			System.out.println("ERROR, NOT A POSSIBLE ACTION");
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * based on the current selected unit, will alter the tiles that a special
	 * or attack can be applied too
	 * 
	 * @param player
	 */
	private void alterTilesToApplyAction(Team player) {
		UnitType unitType = selectedTile.getCurrentUnit().getType();

		/**
		 * if player is passed to the method, the list will update based on the
		 * special ability of the unit. If the special affects walls
		 */
		if (player.equals(Team.PLAYER)) {
			switch (unitType) {
			case ROIDRAGEDIVER:
				// changing to only get tiles without enemies to perform the
				// special
				tilesToApplyAction = map.getTilesInRange(selectedTile
						.getCurrentUnit());
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getTileType()
							.equals(TileType.WALL)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case WEAVER:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.PLAYER)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case TROOPER:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.AI)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case TESLA:
				tilesToApplyAction = map.getTilesInRange(selectedTile
						.getCurrentUnit());
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!(tilesToApplyAction.get(i).getCurrentUnit() == null)
							|| tilesToApplyAction.get(i).getTileType()
									.equals(TileType.WALL)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case ANNOYINGBRAT:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.PLAYER)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case GEARHEAD:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.AI)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case DYLAN:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.AI)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case DRMERCER:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.AI)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case ERIC:
				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
							.equals(Team.AI)) {
						tilesToApplyAction.remove(i);
					}
				}
				break;
			case COMMANDER:
				break;
			default:
				System.out
						.println("ERROR, NOT A VALID TYPE IN ALTERING THE LIST OF APPLIED ACTIONS");
				break;

			}
		}

		/**
		 * if AI is passed into the method, then just keep the enemy units in
		 * the list because attacking is assumed.
		 */
		else {
			for (int i = 0; i < tilesToApplyAction.size(); i++) {
				if (!tilesToApplyAction.get(i).getCurrentUnit().getTeam()
						.equals(Team.AI)) {
					tilesToApplyAction.remove(i);
				} else {
					System.out.println("Friendly unit to removed");
				}
			}
		}
	}

	public void purchaseItem(Item item) {
		state = GameState.EXITING_SHOP;
		// If the player can afford the item
		if (currentPlayer.canBuy(item.getPrice())) {
			// deduct the items price from the players "bank"s
			currentPlayer.buy(item.getPrice());
			selectedTile.getCurrentUnit().addToInventory(item);
			isGameOver = gameScenario.isSatisfied();
			storeIsOpen = false;
			setChanged();
			notifyObservers();
		}

	}

	public void closeTheStore() {
		storeIsOpen = false;
		state = GameState.EXITING_SHOP;
		setChanged();
		notifyObservers();
	}

	public void useItem(Item item) {
		System.out.println("I am using an item!");
		selectedTile.getCurrentUnit().useItem(item);
		state = GameState.EXITING_INVENTORY;
		inventoryIsOpen = false;
		System.out.println("I am updating the observers!");
		setChanged();
		notifyObservers();
	}

	// overloading the method to work with console view
	public void applyAction(int row, int col) {
		state = GameState.SELECTING_UNIT;
		applyAction(map.getTile(row, col));

	}

	public void resetActionPoints() {
		/**
		 * if the current square has a unit, find the unit type and reset the
		 * action points to the base value
		 */
		for (Unit u : currentPlayer.getUnits()) {
			switch (u.getType()) {

			case TROOPER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.TROOPER_ACTION_POINTS);
				break;
			case TESLA:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.TESLA_ACTION_POINTS);
				break;
			case WEAVER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.WEAVER_ACTION_POINTS);
				break;
			case GEARHEAD:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.GEARHEAD_ACTION_POINTS);
				break;
			case ROIDRAGEDIVER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ROID_RAGE_DIVER_ACTION_POINTS);
				break;
			case ANNOYINGBRAT:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ANNOYING_BRAT_ACTION_POINTS);
				break;
			case DRMERCER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.DRMERCER_ACTION_POINTS);
				break;
			case DYLAN:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.DYLAN_ACTION_POINTS);
				break;
			case COMMANDER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.COMMANDER_ACTION_POINTS);
				break;
			case ERIC:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ERIC_ACTION_POINTS);
				break;
			}
		}

		for (Unit u : AI.getUnits()) {
			switch (u.getType()) {

			case TROOPER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.TROOPER_ACTION_POINTS);
				break;
			case TESLA:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.TESLA_ACTION_POINTS);
				break;
			case WEAVER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.WEAVER_ACTION_POINTS);
				break;
			case GEARHEAD:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.GEARHEAD_ACTION_POINTS);
				break;
			case ROIDRAGEDIVER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ROID_RAGE_DIVER_ACTION_POINTS);
				break;
			case ANNOYINGBRAT:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ANNOYING_BRAT_ACTION_POINTS);
				break;
			case DRMERCER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.DRMERCER_ACTION_POINTS);
				break;
			case DYLAN:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.DYLAN_ACTION_POINTS);
				break;
			case COMMANDER:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.COMMANDER_ACTION_POINTS);
				break;
			case ERIC:
				u.setStat(StatType.ACTION_POINTS,
						StatStrategy.ERIC_ACTION_POINTS);
				break;
			}
		}

	}

	/**
	 * pass a tile and apply the action from the selectedTile (the unit on the
	 * tile) to the passed tile.
	 * 
	 * @param tile
	 */
	public void applyAction(Tile tile) {
		state = GameState.SELECTING_UNIT;
		Coordinate cord;
		switch (selectedUnitAction) {
		case SPECIAL:
			specialMediator.performSpecial(selectedTile, tile);
			break;
		case ATTACK:
			attackMediator.battle(selectedTile.getCurrentUnit(),
					tile.getCurrentUnit());

			break;
		case MOVE:
			cord = map.getTileCoordinate(tile);
			map.moveUnit(selectedTile.getCurrentUnit(), cord.getRow(),
					cord.getColumn());
			selectedTile = map.getTile(cord.getRow(), cord.getColumn());
			selectedTile.getCurrentUnit().setStat(
					StatType.ACTION_POINTS,
					selectedTile.getCurrentUnit().getStat(
							StatType.ACTION_POINTS) - 1);
			break;
		case TRADE:
			inventoryIsOpen = true;
			break;
		default:
			System.out.println("ERROR, NOT A POSSIBLE ACTION TO APPLY");
		}
		// Reset everything that needs to be reset
		isGameOver = gameScenario.isSatisfied();
		Unit tradingUnit = null;
		if (selectedUnitAction == ActionType.TRADE) {
			tradingUnit = tile.getCurrentUnit();
		}

		setChanged();
		if (tradingUnit == null) {
			resetSelection();
			notifyObservers();
		} else {
			state = GameState.SELECTING_ITEMS;
			notifyObservers(tradingUnit);
		}
	}

	/**
	 * 
	 */
	public void resetSelection() {
		selectedTile = null;
		selectedUnitAction = null;
		tileActions = null;
		tilesToApplyAction = null;
		map.resetSelection();
	}

	/**
	 * Itterates through the map and clears any unit that was defeated
	 * 
	 * @return
	 */
	private List<Unit> clearDefeatedUnits() {
		return map.clearDefeatedUnits();

	}

	@Override
	public String toString() {
		String result = "";
		result += map.toString();
		if (selectedTile != null) {
			// Add the stats to the toString
			result += "\n";
			// Add the name of the tile
			result += selectedTile.getName() + "\n";
			result += statsToString(selectedTile.getStatModifiers());
			result += "\n";
			if (selectedTile.getCurrentUnit() != null) {
				result += selectedTile.getCurrentUnit().getName() + "\n";
				result += statsToString(selectedTile.getCurrentUnit()
						.getStatList());
			}

		}
		return result;

	}

	/**
	 * Gets a toString version of the stats for a selected tile
	 * 
	 * @param statModifiers
	 * @return
	 */
	private String statsToString(List<StatNode> statModifiers) {
		String result = "";
		for (int index = 0; index < statModifiers.size(); index++) {
			result += statModifiers.get(index).getStatNameType() + ": "
					+ statModifiers.get(index).getStatModification();
			result += "\n";
		}
		return result;
	}

	public String getObjectives() {
		String objectives = "";
		objectives = GameStrategy.PRIMARY_GAME_OBJECTIVE;
		objectives = "\n";
		objectives = GameStrategy.SECONDARY_GAME_OBJECTIVE;
		objectives = "\n";
		objectives = GameStrategy.TERTIARY_GAME_OBJECTIVE;

		return objectives;

	}

	/**
	 * the attackMediator calculates the damage to a unit, based on a integer
	 * from 0-5, + the attacking units attack score, minus the defending units
	 * defense
	 * 
	 */

	private class AttackMediator implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AttackMediator() {
		}

		public void battle(Unit attackingUnit, Unit defendingUnit) {
			Random damageDeviation = new Random();
			int damageBonus = damageDeviation.nextInt(6);
			int damage = ((attackingUnit.getStat(StatType.ATTACK) + damageBonus) - defendingUnit
					.getStat(StatType.DEFENSE));
			if (damage <= 0) {
				damage = 1;
			}
			defendingUnit.setStat(StatType.HP,
					defendingUnit.getStat(StatType.HP) - damage);

			// Determine if the unit needs to level up
			ExperienceStrategy.calculateLevel(attackingUnit, defendingUnit,
					damage);

			attackingUnit.setStat(StatType.ACTION_POINTS,
					attackingUnit.getStat(StatType.ACTION_POINTS) - 1);

			List<Unit> defeatedUnits = clearDefeatedUnits();
			if (defeatedUnits.size() != 0) {
				// Go through the list and remove the units from the correct
				// player
				for (Unit defeatedUnit : defeatedUnits) {
					if (defeatedUnit.getTeam() == Team.AI) {
						AI.getUnits().remove(defeatedUnit);
						// Add money to the player accoarding to the formula in
						// the MoneyStrategy
						currentPlayer.addToMoney(MoneyStrategy
								.calculateMoneyGain(defeatedUnit));
					} else {
						currentPlayer.getUnits().remove(defeatedUnit);
						AI.addToMoney(MoneyStrategy
								.calculateMoneyGain(defeatedUnit));
					}
				}
			}
		}
	}

	private class SpecialMediator implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SpecialMediator() {
		}

		public void performSpecial(Tile changer, Tile tileToBeChanged) {
			UnitType unitType = changer.getCurrentUnit().getType();
			Random rand = new Random();
			int statBonus = rand.nextInt(3) + 1;
			int healthBonus = rand.nextInt(10) + 1;
			Coordinate cord;

			switch (unitType) {
			case ROIDRAGEDIVER:
				// changing the selected wall Tile to to rubble
				cord = map.getTileCoordinate(tileToBeChanged);
				Unit unitToSet = map.getTile(cord.getRow(), cord.getColumn()).getCurrentUnit();
				Tile tileToSet = new Rubble(TileType.RUBBLE);
				tileToSet.setCurrentUnit(unitToSet);
				map.setTile(cord.getRow(), cord.getColumn(), tileToSet);

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit()
						.setStat(
								StatType.ACTION_POINTS,
								changer.getCurrentUnit().getStat(
										StatType.ACTION_POINTS)
										- StatStrategy.ROID_RAGE_DIVER_SPECIAL_ACTION_USAGE);
				break;
			case WEAVER:
				// increasing attack of the unit
				tileToBeChanged.getCurrentUnit().setStat(
						StatType.ATTACK,
						tileToBeChanged.getCurrentUnit().getStat(
								StatType.ATTACK)
								+ statBonus);

				// increasing defense of the unit
				tileToBeChanged.getCurrentUnit().setStat(
						StatType.DEFENSE,
						tileToBeChanged.getCurrentUnit().getStat(
								StatType.DEFENSE)
								+ statBonus);

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.WEAVER_SPECIAL_ACTION_USAGE);
				break;
			case TROOPER:
				// decreasing attack of the enemy unit
				tileToBeChanged.getCurrentUnit().setStat(
						StatType.ATTACK,
						tileToBeChanged.getCurrentUnit().getStat(
								StatType.ATTACK)
								- statBonus);

				// decreasing defense of the enemy unit
				tileToBeChanged.getCurrentUnit().setStat(
						StatType.DEFENSE,
						tileToBeChanged.getCurrentUnit().getStat(
								StatType.DEFENSE)
								- statBonus);

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.TROOPER_SPECIAL_ACTION_USAGE);
				break;
			case TESLA:
				// changing the selected tile to be a tesla coil trap
				cord = map.getTileCoordinate(tileToBeChanged);
				map.setTile(cord.getRow(), cord.getColumn(), new TeslaCoil(
						TileType.TESLA_COIL));

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.TESLA_SPECIAL_ACTION_USAGE);
				break;
			case ANNOYINGBRAT:
				// increasing selected units health
				tileToBeChanged.getCurrentUnit().setStat(
						StatType.HP,
						tileToBeChanged.getCurrentUnit().getStat(StatType.HP)
								+ healthBonus);

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit()
						.setStat(
								StatType.ACTION_POINTS,
								changer.getCurrentUnit().getStat(
										StatType.ACTION_POINTS)
										- StatStrategy.ANNOYING_BRAT_SPECIAL_ACTION_USAGE);
				break;
			case GEARHEAD:

				// adding 10 for a special attack onto the enemy on the
				// tileToBeChanged
				tileToBeChanged
						.getCurrentUnit()
						.setStat(
								StatType.HP,
								tileToBeChanged.getCurrentUnit().getStat(
										StatType.HP)
										- ((changer.getCurrentUnit().getStat(
												StatType.ATTACK) + 10) - tileToBeChanged
												.getCurrentUnit().getStat(
														StatType.DEFENSE)));

				// decreasing the health by 5 after the attack
				changer.getCurrentUnit().setStat(StatType.HP,
						changer.getCurrentUnit().getStat(StatType.HP) - 5);

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.GEARHEAD_SPECIAL_ACTION_USAGE);
				break;
			case DYLAN:
				// decreasing defense to 0, because everyone lets their guard
				// down
				// around Dylan
				tileToBeChanged.getCurrentUnit().setStat(StatType.DEFENSE, 0);
				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.DYLAN_SPECIAL_ACTION_USAGE);
				break;
			case DRMERCER:
				int percentage = rand.nextInt(100) + 1;

				// Dr mercer has a 1 percent chance to completely removed a unit
				// from the field
				// otherwise he removes himself
				if (percentage == 1) {
					map.removeUnit(tileToBeChanged.getCurrentUnit());
				} else
					// decreasing the action points by the appropriate amount
					changer.getCurrentUnit()
							.setStat(
									StatType.ACTION_POINTS,
									changer.getCurrentUnit().getStat(
											StatType.ACTION_POINTS)
											- StatStrategy.DRMERCER_SPECIAL_ACTION_USAGE);

				map.removeUnit(changer.getCurrentUnit());
				break;
			case ERIC:
				// eric will not use the passed unit, even though he has to
				// select a unit in range
				// will decrease all units in his range by half

				for (int i = 0; i < tilesToApplyAction.size(); i++) {
					tilesToApplyAction
							.get(i)
							.getCurrentUnit()
							.setStat(
									StatType.HP,
									tilesToApplyAction.get(i).getCurrentUnit()
											.getStat(StatType.HP) / 2);
				}

				// decreasing the action points by the appropriate amount
				changer.getCurrentUnit().setStat(
						StatType.ACTION_POINTS,
						changer.getCurrentUnit()
								.getStat(StatType.ACTION_POINTS)
								- StatStrategy.ERIC_SPECIAL_ACTION_USAGE);
				break;
			default:
				System.out
						.println("ERROR, NOT A VALID TYPE IN SPECIAL MEDIATOR");
				break;

			}
			List<Unit> defeatedUnits = clearDefeatedUnits();
			if (defeatedUnits.size() != 0) {
				// Go through the list and remove the units from the correct
				// player
				for (Unit defeatedUnit : defeatedUnits) {
					if (defeatedUnit.getTeam() == Team.AI) {
						AI.getUnits().remove(defeatedUnit);
					} else {
						currentPlayer.getUnits().remove(defeatedUnit);
					}
				}
			}
		}

	}

	private class Scenario implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private ScenarioType type;
		private Unit objectiveUnit;
		private Item objectiveItem;
		private List<Unit> escapedUnits;

		/**
		 * Instantiates the type and assigns values accordingly.
		 * 
		 * @param ScenarioType
		 */

		public Scenario(ScenarioType type) {
			this.type = type;
			switch (type) {
			case BUY_DRAGONBALLS:
				objectiveItem = ScenarioStrategy.OBJECTIVE_ITEM;
				break;
			case KILL_COMMANDER:
				objectiveUnit = ScenarioStrategy.OBJECTIVE_UNIT;
				break;
			case ESCAPE:
				escapedUnits = new LinkedList<>();
			default:
				break;
			}

		}

		/**
		 * All check whether all enemies are dead and the second condition
		 * 
		 * @return a boolean indicating whether the conditions have been met
		 */

		public boolean isSatisfied() {
			switch (type) {
			case BUY_DRAGONBALLS:
				return checkForDragonBalls() || checkAllUnits();
			case KILL_COMMANDER:
				return checkCommander() || checkAllUnits();
			case ESCAPE:
				return checkEscape() || checkAllUnits();
			default:
				break;
			}
			return false;
		}

		/**
		 * checks each item in the entire inventory
		 * 
		 * @return true if the user has the Dragonballs
		 */
		// Can someone please make an overall inventory of the player?
		// That way I can iterate only through one list and not many.

		private boolean checkForDragonBalls() {

			for (Item item : currentPlayer.getInventory()) {
				if (item.getName().equals(objectiveItem.getName())) {
					state = GameState.WON;
					return true;
				}
			}
			for (Item item : AI.getInventory()) {
				if (item.getName().equals(objectiveItem.getName())) {
					state = GameState.LOST;
					return true;
				}
			}

			return false;
		}

		/**
		 * If there are no more enemies in the list, then return true Otherwise,
		 * return false
		 * 
		 * @return true or false accordingly
		 */

		public boolean checkAllUnits() {

			if (AI.getUnits().isEmpty()) {
				state = GameState.WON;
				return true;
			} else if (currentPlayer.getUnits().isEmpty()) {
				state = GameState.LOST;
				return true;
			}
			return false;

		}

		/**
		 * Iterates through the list of enemies to check if the Commander is
		 * still in the list.
		 * 
		 * If the Commander is in the list, then we return false because the win
		 * condition has been not met.
		 * 
		 * Otherwise, this method will return true.
		 * 
		 * @return true or false accordingly
		 */

		public boolean checkCommander() {
			for (Unit unit : AI.getUnits()) {
				if (unit.getType() == objectiveUnit.getType()) {
					return false;
				}
			}
			state = GameState.WON;
			return true;

		}

		public boolean checkEscape() {

			if (currentPlayer.getUnits().isEmpty() && !escapedUnits.isEmpty()) {
				state = GameState.WON;
				return true;
			}
			return false;

		}

	}

	private class AI extends Player implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		// boolean temp = false;
		Random chance;

		/**
		 * Sets the AI type for this AI
		 */
		public AI() {
			super();
			chance = new Random();
			// Gerenate the units
			generateUnits();
			// If the scenario requires a commander then add on to the AI
			// units
			if (scenarioType == ScenarioType.KILL_COMMANDER) {
				unitList.add(Commander.getInstance());
			}
			for (Unit unit : unitList) {
				unit.setTeam(Team.AI);
			}
		}

		private void generateUnits() {
			int unitCount = 0;
			while (unitCount < 5) {
				int randomNumber = chance.nextInt(100);
				if (randomNumber >= 0 && randomNumber < 30) {
					unitList.add(new Gearhead());
				} else if (randomNumber >= 30 && randomNumber < 40) {
					AnnoyingBrat cindy = new AnnoyingBrat();
					unitList.add(cindy);
				} else if (randomNumber >= 40 && randomNumber < 50) {
					unitList.add(new Tesla());
				} else if (randomNumber >= 50 && randomNumber < 70) {
					unitList.add(new Trooper());
				} else if (randomNumber >= 70 && randomNumber < 90) {
					unitList.add(new Weaver());
				} else if (randomNumber >= 90 && randomNumber < 100) {
					unitList.add(new RoidRageDiver());
				}
				unitCount++;
			}

		}

		public void act() {

			// TODO: The AI has to move and has a 50/50 chance of attacking.
			// Otherwise, it does nothing.

			// So what we need to do is itterate through the list of units
			// that
			// this AI has and for each one of them we need to determine if
			// there is a unit for the unit to attack. If there is a unit
			// then
			// there is a 50% chance the the AI will attack, otherwise the
			// unit
			// will do nothing. If the unit cannot attack a unit then this
			// unit
			// should advance as far as it can

			// Iterate through the list of units
			for (Unit unit : unitList) {
				// Whenever we select a unit we are going to try to buy
				// something for it
				// Open the store and see if any items can be purchased
				for (Item i : store.getItems().getAllItems()) {
					if (scenarioType == ScenarioType.BUY_DRAGONBALLS) {
						if (i.getType() == ItemType.DRAGONBALLS
								&& i.getPrice() <= money) {
							money -= i.getPrice();
							inventory.add(i);
						}
					} else {
						if (i.getPrice() <= money) {
							unit.addToInventory(i);
							money -= i.getPrice();
							break;
						}
					}
				}

				if (!unit.getInventory().getAllItems().isEmpty()) {
					unit.useItem(unit.getInventory().get(0));
				}
				// Now we determine what to do
				Coordinate unitLocation = map.getUnitCoordinate(unit);
				int row = unitLocation.getRow();
				int col = unitLocation.getColumn();
				List<Tile> unitsInRange = map.getUnitsInRange(unit);
				if (unitsInRange.size() == 0) {
					// There are no units in range
					// So move randomly in one direction
					moveUnit(unit, row, col);
				} else {
					// So there are some units, now we need to detemrine if
					// they
					// are enemy units
					int numEnemies = 0;
					for (Tile tileInRange : unitsInRange) {
						Unit unitInRange = tileInRange.getCurrentUnit();
						if (unitInRange.isEnemy(unit.getTeam())) {
							numEnemies++;
							// Coordinate enemyCoordinate =
							// map.getCoordinate(unitInRange);

							AttackMediator attackMediator = new AttackMediator();
							attackMediator.battle(unit, unitInRange);

							break;
						}
					}
					if (numEnemies == 0) {
						moveUnit(unit, row, col);
					}
				}
			}

			resetActionPoints();
			resetSelection();
			store.shuffleItems();
			isGameOver = gameScenario.isSatisfied();
			setChanged();
			notifyObservers();
		}

		/**
		 * @param unit
		 * @param row
		 * @param col
		 */
		public void moveUnit(Unit unit, int row, int col) {
			List<Tile> movementArea = map.getPossibleMovementTiles(unit);
			Random movementRandom = new Random();
			Tile selectedTile = movementArea.get(movementRandom
					.nextInt(movementArea.size()));
			map.moveUnit(unit, map.getTileCoordinate(selectedTile).getRow(),
					map.getTileCoordinate(selectedTile).getColumn());
		}

	}

	/**
	 * Returns the current player playing the game.
	 * 
	 * @return
	 */
	public Player getCurrentPlayer() {

		return currentPlayer;
	}

	public List<Item> getPlayerInventory() {
		return selectedTile.getCurrentUnit().getInventory().getAllItems();
	}

	public Coordinate getCoordinate(Tile tile) {
		return map.getTileCoordinate(tile);
	}

	/**
	 * Simulates the player ending a turn. Essentially, the AI makes a move at
	 * this point
	 */
	public void endTurn() {
		resetActionPoints();
		resetSelection();
		store.shuffleItems();
		state = GameState.SELECTING_UNIT;
		AI.act();
		turnCount++;
	}

	public boolean isInventoryIsOpen() {
		return inventoryIsOpen;
	}

	public Tile[][] getGameAsArray() {
		return map.getMapAsArray();
	}

	public MapType getMapType() {
		return mapType;
	}

	public void closeInventory() {
		resetSelection();
		state = GameState.EXITING_INVENTORY;
		inventoryIsOpen = false;
		setChanged();
		notifyObservers();
	}
}
